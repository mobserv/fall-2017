Name
Seed

Slogan
Think it. Seed it. Grow it.

Platform
iOS

Short description
This is the app for finding and creating nearby events! Create an event and watch it grow on the map as it gains popularity and like the events you would like to attend to keep track and increase the popularity.

Implemented features
Map view of events:
Toggle map type
Event clustering

Filter the events on:
Category
Liked events
Created events
Create events

Users can create their own events in a desired location, and add the following information:
Name
Detailed description
Start & end time
Category
View events
Users can get both non-detailed and detailed information about events created by others.
Events that has ended stays visible on the map for seven days. 
Non-detailed view provides information about:
Name
Place
Number of likes
Like button
Detailed view provides additional information such as:
Detailed description
Start & end time
Ongoing events allows users to:
Take a photo and add it to the event
View the photo gallery of an event (also allowed for finished events)

Used frameworks
Firebase
Cluster
Mapkit
IQKeyboardManager

Required permissions

