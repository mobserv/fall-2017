package eurecom.fr.achievements;

import java.io.IOException;
import com.google.appengine.api.datastore.Entity;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.KeyFactory;

public class DeleteAchievementServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity achiev = null;
		try {
			achiev = datastore.get(KeyFactory.stringToKey(req.getParameter("id")));
			} catch (Exception e) {
			resp.getWriter().println("Sorry, no achievement for the given key");
			return;
			}
		resp.getWriter().println("Achievement with key: " + achiev.getKey() + " was successfully deleted!");
		
		
		datastore.delete(KeyFactory.stringToKey(req.getParameter("id")));
	}
}
