package eurecom.fr.achievements;

import java.io.IOException;
import java.io.PrintWriter;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.blobstore.*;
import com.google.appengine.api.files.*;
import com.google.appengine.api.images.*;
import java.util.List;
import java.util.ArrayList;
import java.util.*;


public class AddAchievementServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		// Let's output the basic HTML headers
		PrintWriter out = resp.getWriter();
		out.println("<html><head><title>Modify an Achievement</title></head><body>");
		// Get the datastore
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// Get the entity by key
		Entity achiev = null;
		String title = "", pict = "", points = "", tags = "";
		
		try {
			achiev = datastore.get(KeyFactory.stringToKey(req.getParameter("id")));
			title = (achiev.getProperty("title") != null) ? (String) achiev.getProperty("title") : "";
			points = (achiev.getProperty("points") != null) ? (String) achiev.getProperty("points") : "";
			pict = (achiev.getProperty("pict") != null) ? (String) achiev.getProperty("pict") : "";
			tags = (achiev.getProperty("tags") != null) ? (String) achiev.getProperty("tags") : "";
			} catch (EntityNotFoundException e) {
			resp.getWriter().println("<p>Creating a new Achievement</p>");
			} catch (NullPointerException e) {
			// id paramenter not present in the URL
			resp.getWriter().println("<p>Creating a new Achievement</p>");
			} catch (com.google.appengine.api.datastore.EntityNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		out.println("<form action=\"add\" method=\"post\" title=\"achievement\">");
		// Let's build the form
		out.print("<label>Title: </label><input name=\"title\" value=\"" + title + "\"/><br/>"
		+ "<label>Points: </label><input name=\"points\" value=\"" + points + "\"/><br/>"
		+ "<label>Picture: </label><input name=\"pict\" value=\"" + pict + "\"/><br/>"
		+ "<label>Tags: </label><input name=\"tags\" value=\"" + tags + "\"/><br/>");
		out.println("<br/><input type=\"submit\" value=\"Continue\"/></form></body></html>");
		}

	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// Retrieve informations from the request
		
		
		
		
		String achievTitle = req.getParameter("title");
		String achievPict = req.getParameter("pict");
		String achievPoints = req.getParameter("points");
		String achievTags = req.getParameter("tags");
		// Take a reference of the datastore
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// Generate or retrieve the key associated with an existent contact
		// Create or modify the entity associated with the contact
		Entity achiev;
		achiev = new Entity("Achievement", achievPict);
		achiev.setProperty("title", achievTitle);
		achiev.setProperty("pict", achievPict);
		achiev.setProperty("points", achievPoints);
		achiev.setProperty("tags", achievTags);
		// Save in the Datastore
		datastore.put(achiev);
		resp.getWriter().println("Achievement " + achievTitle + " saved with key " +
				KeyFactory.keyToString(achiev.getKey()) + "!");
		}
}
