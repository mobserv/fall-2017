package eurecom.fr.achievements;

import java.io.IOException;
import javax.servlet.http.*;
import java.util.Enumeration;

@SuppressWarnings("serial")
public class AchievementsServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		String msg = "It's Achievements!";
		resp.getWriter().println("ok");
		Enumeration<String> parameterNames = req.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			resp.getWriter().println(paramName);
			resp.getWriter().println("ok");
		}
		
		
		resp.getWriter().println(msg);
	}
}
