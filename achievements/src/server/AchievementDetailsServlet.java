package eurecom.fr.achievements;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class AchievementDetailsServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if (req.getParameter("id") == null) {
			resp.getWriter().println("ID cannot be empty!");
			return;
			}
			// Get the datastore
			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			// Get the entity by key
			Entity achiev = null;
			try {
			achiev = datastore.get(KeyFactory.stringToKey(req.getParameter("id")));
			} catch (Exception e) {
			resp.getWriter().println("Sorry, no achievement for the given key");
			return;
			}
			// Let's output the basic HTML headers
			PrintWriter out = resp.getWriter();
			
			/** Different response type? */
			String responseType = req.getParameter("respType");
			if (responseType != null) {
			 if (responseType.equals("json")) {
			// Set header to JSON output
			resp.setContentType("application/json");
			out.println(getJSON(achiev, req, resp));
			return;
			} else if (responseType.equals("xml")) {
			resp.setContentType("application/json");
			 out.println(getXML(achiev, req, resp));
			 return;
			 }
			}
		
			resp.setContentType("text/html");
			out.println("<html><head><title>achievement list</title></head><body>");
			// Let's build the table headers
			out.println("<table style=\"border: 1px solid black; width: auto; text-align: center;\">");
			out.print("<td>Title: </td><td>" + achiev.getProperty("title") + "</td></tr>"
			+ "<td>Picture: </td><td>" + achiev.getProperty("pict") + "</td></tr>"
			+ "<tr><td>Points: </td><td>" + achiev.getProperty("points") + "</td></tr>"
			+ "<tr><td>Tags: </td><td>" + achiev.getProperty("tags") + "</td></tr>");
			out.println( "</table><a href=\"delete?id=" + KeyFactory.keyToString(achiev.getKey()) + "\">Delete</a>");
			out.println("<a href=\"add?id=" + req.getParameter("id")
			+ "\">Modify</a></body></html>");
			
			
			
			//out.println("<br/><input type=\"submit\" value=\"Continue\"/></form></body></html>");

	}
	
	private String getXML(Entity achiev, HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		return null;
		}
	
	private String getJSON(Entity achiev, HttpServletRequest req, HttpServletResponse resp) {
		// Create a JSON array that will contain all the entities converted in a JSON version
	
		JSONObject contactJSON = new JSONObject();
		try {
		contactJSON.put("title", achiev.getProperty("title"));
		contactJSON.put("points", achiev.getProperty("points"));
		contactJSON.put("pict", achiev.getProperty("pict"));
		contactJSON.put("id", KeyFactory.keyToString(achiev.getKey()));
		contactJSON.put("tags", achiev.getProperty("tags"));
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		return contactJSON.toString();
		}
}
