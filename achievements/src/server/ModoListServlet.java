package eurecom.fr.achievements;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class ModoListServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		// Take the list of contacts ordered by name
		Query query = new Query("Achievement").addSort("title", Query.SortDirection.ASCENDING);
		List<Entity> achievsall = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
		List<Entity> achievs = new ArrayList<Entity>();
		
		// Let's output the basic HTML headers
		PrintWriter out = resp.getWriter();
		
		/** Different response type? */
		String responseType = req.getParameter("respType");
		if (responseType != null) {
		 if (responseType.equals("json")) {
		// Set header to JSON output
		resp.setContentType("application/json");
		out.println(getJSON(achievsall, req, resp));
		return;
		} else if (responseType.equals("xml")) {
		resp.setContentType("application/json");
		 out.println(getXML(achievsall, req, resp));
		 return;
		 }
		}
		
		
		resp.setContentType("text/html");
		out.println("<html><head><title>Achievements for Modos list</title></head><body>");
		
		// Have all achievements we want modo
		if (achievsall.isEmpty()) {
			 out.println("<h1>Your list of moderation is empty!</h1>");
			} else {
				for (Entity achiev: achievsall) {
					if(achiev.getProperty("points").equals("-1")){
						achievs.add(achiev);
					}
				}
				
				if (achievs.isEmpty()) {
					 out.println("<h1>Your list of moderation is empty!</h1>");
					} else {
					
						// Let's build the table headers
					 out.println("<table style=\"border: 1px solid black; width: 100%; text-align: center;\">"
					 + "<tr><th>Title</th><th>Points</th><th>Details</th></tr>");
					 for (Entity achiev: achievs) {
						 
							 out.println("<tr><td>" + achiev.getProperty("title") + "</td>"
									 + "<td>" + achiev.getProperty("points") + "</td>"
									 + "<td><a href=\"achievementdetails?id="
									 + KeyFactory.keyToString(achiev.getKey())
									 + "\">details</a></td>"
									 + "</tr>");
						 }
						 out.println("</table>");
					 }
					 out.println("</body></html>");
					
			}
		
		
		
	}
	
	private String getXML(List<Entity> achievs, HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		return null;
		}
	
	private String getJSON(List<Entity> achievs, HttpServletRequest req, HttpServletResponse resp) {
		// Create a JSON array that will contain all the entities converted in a JSON version
		JSONArray results = new JSONArray();
		List<Entity> achievsmodo = new ArrayList<Entity>();
		for (Entity achiev: achievs) {
			if(achiev.getProperty("points").equals("-1")==true){
				achievsmodo.add(achiev);
			} 
		}
		
		for (Entity achiev: achievsmodo) {
		 JSONObject contactJSON = new JSONObject();
		try {
		contactJSON.put("title", achiev.getProperty("title"));
		contactJSON.put("points", achiev.getProperty("points"));
		contactJSON.put("pict", achiev.getProperty("pict"));
		contactJSON.put("id", KeyFactory.keyToString(achiev.getKey()));
		contactJSON.put("tags", achiev.getProperty("tags"));
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		results.put(contactJSON);
		}
		return results.toString();
		}
}
