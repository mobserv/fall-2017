package redfoxes.achievements.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import redfoxes.achievements.R;

/**
 * Created by paulj on 11/12/2017.
 */

public class TagView extends FrameLayout {
    private boolean selected;
    private String name;
    private ImageView imageTag;
    private Context context;
    private String definition;
    private FrameLayout frameLayout;

    public TagView(Context newContext, String name, boolean iSselected, final boolean selectable) {
        super(newContext);
        this.context=newContext;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.from(context).inflate(R.layout.tag_view_layout, this, true);
        this.selected = iSselected;
        this.name = name;
        imageTag = (ImageView) findViewById(R.id.image_imageView_tag);
        frameLayout = (FrameLayout) findViewById(R.id.FrameLayout__imageTag);
        findImageAndDef();
        if(selectable){
            imageTag.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickOnTag();
                }
            });
        }
        imageTag.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(context, definition, Toast.LENGTH_SHORT ).show();
                return true;
            }
        });
        if(!isSelected()){
            imageTag.setAlpha((float) 0.5);
        }
    }

    private void findImageAndDef(){
        switch(name) {
            case "sport":
                imageTag.setImageResource(R.drawable.sport_tag);
                definition= getResources().getString(R.string.sport);
                break;
            case "food":
                imageTag.setImageResource(R.drawable.food_tag);
                definition= getResources().getString(R.string.food);
                break;
            case "extrem":
                imageTag.setImageResource(R.drawable.extrem_tag);
                definition= getResources().getString(R.string.extrem);
                break;
            case "work":
                imageTag.setImageResource(R.drawable.work_tag);
                definition= getResources().getString(R.string.work);
                break;
            case "diy":
                imageTag.setImageResource(R.drawable.diy_tag);
                definition= getResources().getString(R.string.diy);
                break;
            case "social":
                imageTag.setImageResource(R.drawable.social_tag);
                definition= getResources().getString(R.string.social);
                break;
            case "travel":
                imageTag.setImageResource(R.drawable.travel_tag);
                definition= getResources().getString(R.string.travel);
                break;
            case "cultural":
                imageTag.setImageResource(R.drawable.cultural_tag);
                definition= getResources().getString(R.string.cultural);
                break;
            case "art":
                imageTag.setImageResource(R.drawable.art_tag);
                definition= getResources().getString(R.string.art);
                break;
            case "game":
                imageTag.setImageResource(R.drawable.game_tag);
                definition= getResources().getString(R.string.game);
                break;
            case "luck":
                imageTag.setImageResource(R.drawable.luck_tag);
                definition= getResources().getString(R.string.luck);
                break;
            case "event":
                imageTag.setImageResource(R.drawable.event_tag);
                definition= getResources().getString(R.string.event);
                break;
            case "X":
                imageTag.setImageResource(R.drawable.x_tag);
                definition= getResources().getString(R.string.x);
                break;
            default:
                imageTag.setImageResource(R.drawable.ic_info_black_24dp);
                definition= getResources().getString(R.string.app_name);
        }
        if(name.equals("X")){
            Log.i("Tag","pink");
            imageTag.setColorFilter(ContextCompat.getColor(context, R.color.pinkX), android.graphics.PorterDuff.Mode.SRC_IN);
        }else {
            imageTag.setColorFilter(ContextCompat.getColor(context, R.color.Achievement_green_dark), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    public boolean isSelected() {
        return selected;
    }

    public String getName() {
        return name;
    }

    public ImageView getImageTag() {
        return imageTag;
    }

    public void clickOnTag(){
        imageTag.setAlpha((float) ((float) Math.abs(imageTag.getAlpha()-1.0)+0.5));
        selected=!selected;
    }

    public void setSize(int size){
        frameLayout.getLayoutParams().height = size;
        frameLayout.getLayoutParams().width = size+4;
    }
}
