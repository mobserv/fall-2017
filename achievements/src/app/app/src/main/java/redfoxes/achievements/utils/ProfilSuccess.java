package redfoxes.achievements.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by paulj on 11/12/2017.
 */

public class ProfilSuccess implements Serializable {

    private Achievement achiev;
    private Date date;
    private String comment;

    public ProfilSuccess(Achievement achiev, Date date, String comment) {
        this.achiev = achiev;
        this.date = date;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public Date getDate() {
        return date;
    }

    public Achievement getAchiev() {
        return achiev;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
