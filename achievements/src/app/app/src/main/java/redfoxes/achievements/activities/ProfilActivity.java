package redfoxes.achievements.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.Profil;
import redfoxes.achievements.views.TopbarView;


public class ProfilActivity extends AppCompatActivity {

    private Profil profil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.topbarslot_FrameLayout__profil);
        frameLayout.addView(new TopbarView(this,true));
        profil = (Profil) getIntent().getSerializableExtra("profil");
        ImageView Pict = (ImageView) findViewById(R.id.picture_ImageView__profil);
        Pict.setImageBitmap(profil.getBitmapPict());
    }


}
