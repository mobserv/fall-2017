package redfoxes.achievements.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import redfoxes.achievements.R;
import redfoxes.achievements.activities.AchievList;
import redfoxes.achievements.activities.Success;
import redfoxes.achievements.views.TagView;

public class AchievementAdapter extends ArrayAdapter<Achievement>{
    private final Context context;
    private final int id;
    private final List<Achievement> ach_list;
    private final AchievList acl;
    private Achievement achiev;
    private ArrayList<TagView> arrayTagsView = new ArrayList<TagView>();
    private ImageView Button_validate;
    private ImageView Button_signaled;
    private ImageView Button_later;
    private ImageView Button_hide;

    public AchievementAdapter(Context context, int id, List<Achievement> ach_list, AchievList acl)
    {
        super(context,id,ach_list);
        this.context = context;
        this.acl=acl;
        this.id = id;
        this.ach_list = ach_list;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) throws NoSuchElementException
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(id, parent, false);

        achiev = ach_list.get(position);
        TextView achievName= (TextView) rowView.findViewById(R.id.textView);
        TextView achievPoints= (TextView) rowView.findViewById(R.id.textView2);
        ImageView achievPict= (ImageView) rowView.findViewById(R.id.logo_imageView__achListItemView);
        LinearLayout achievTags = (LinearLayout) rowView.findViewById(R.id.list_tags_achiev);

        String tagName;
        achievName.setText(achiev.getTitle());
        String points_str = Integer.toString(achiev.getPoints())+" points";
        achievPoints.setText(points_str);
        achievPict.setImageBitmap(achiev.getPictBitmap());
        Iterator i = achiev.getTags().iterator();
        while(i.hasNext()){
            tagName = (String)i.next();
            TagView nTag = new TagView(getContext(),tagName,false,false);
            nTag.setSize(30);
            //arrayTagsView.add(nTag);
            achievTags.addView(nTag);
        }
        Button_validate = (ImageView) rowView.findViewById(R.id.button_validation);
        if(ach_list.get(position).isDone()){
            //Button_validate.setAlpha((float) 0.5);
        }
        Button_validate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,Success.class);
                intent.putExtra("achievement",ach_list.get(position));
                ach_list.get(position).setDone(true);
                //Button_validate.setAlpha((float) ((float) Math.abs(Button_validate.getAlpha()-1.0)+0.5));
                Toast.makeText(context, "You've done this", Toast.LENGTH_SHORT ).show();
                view.getContext().startActivity(intent);
                acl.actualizefilteredList();
            }
        });
        Button_signaled = (ImageView) rowView.findViewById(R.id.button_signal);
        if(ach_list.get(position).isSignaled()){
            Button_signaled.setAlpha((float) 1.0);
        }
        Button_signaled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO add to hidden ones
                Button_signaled.setAlpha((float) ((float) Math.abs(Button_signaled.getAlpha()-1.0)+0.5));
                ach_list.get(position).setSignaled(!ach_list.get(position).isSignaled());
                Toast.makeText(context, "You signal this", Toast.LENGTH_SHORT ).show();
                acl.actualizefilteredList();
            }
        });
        final ImageView Button_later = (ImageView) rowView.findViewById(R.id.button_willdoit);
        if(ach_list.get(position).isLater()){
            Button_later.setAlpha((float) 1.0);
        }
        Button_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ach_list.get(position).setLater(!ach_list.get(position).isLater());
                Button_later.setAlpha((float) ((float) Math.abs(Button_later.getAlpha()-1.0)+0.5));
                Toast.makeText(context, "Add to later list", Toast.LENGTH_SHORT ).show();
                acl.actualizefilteredList();
            }
        });
        final ImageView Button_hide = (ImageView) rowView.findViewById(R.id.button_notinterested);
        if(ach_list.get(position).isHide()){
            Toast.makeText(context, ach_list.get(position).getTitle(), Toast.LENGTH_SHORT ).show();
            Button_hide.setAlpha((float) 1.0);
        }
        Button_hide.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO add to hidden ones
            ach_list.get(position).setHide(!ach_list.get(position).isHide());
            Button_hide.setAlpha((float) ((float) Math.abs(Button_hide.getAlpha()-1.0)+0.5));
            Toast.makeText(context, "You hide this", Toast.LENGTH_SHORT ).show();
            acl.actualizefilteredList();
        }
    });

        return rowView;
    }

    private int dpToPx(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

}

//JSON Loader:
//achievement = id;title;pict;points;tags