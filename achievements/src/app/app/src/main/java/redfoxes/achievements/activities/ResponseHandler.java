package redfoxes.achievements.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class ResponseHandler {
    private final String url = "http://1-dot-newachievements-189013.appspot.com/";

    private Context context;
    private String request;
    private String format;
    private Handler handler;
    private Activity activity;
    private Map<String,String> data;
    private int statusCode;

    public Activity getActivity() {
        return activity;
    }

    public ResponseHandler(Context context, String request, String format, Handler handler,
                           Activity activity, Map<String,String> data)
    {
        this.context = context;
        this.request = url + request;
        this.format = format;
        this.handler = handler;
        this.activity = activity;
        this.data = data;
    }

    public void finish()
    {
        handler.post(new Runnable() {
            public void run() {
                activity.finish();
            }
        });
    }

    private void popUp(String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public String start()
    {
        String out = null;
        if(format.compareTo("POST") == 0)
            try {
                out = postData();
                Log.i("Response==", out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        else{
            try {
                deleteData();
            }catch (Exception e){


            }
        }
        return out;
    }

    public void respond(String response) {
 /* To be implemented
 * If the statusCode is ok?
 * Notify the user about the successful operation
 * If not simply state that a problem has occurred
 * Remember to conclude the activity
 * */
        switch (response) {
            case "200":
                popUp("Modifications saved");
                break;
            case "201":
                popUp("New resource has been created");
                break;
            case "202":
                popUp("Processing has not been completed");
                break;
            case "203":
                popUp("Modifications saved");
                break;
            case "206":
                popUp("Not all data were saved on server");
                break;
            case "OK":
                popUp("Modifications saved");
                break;
            case "301":
                popUp("Resource was definitely removed");
                break;
            case "302":
                popUp("Resource was temporarily removed");
                break;
            case "400":
                popUp("Error in syntax");
                break;
            case "401":
                popUp("User authentication was required");
                break;
            case "403":
                popUp("Non authorized request");
                break;
            case "404":
                popUp("The demanded resource was not found");
                break;
            case "405":
                popUp("Modifications not allowed");
                break;
            case "406":
                popUp("Not acceptable request");
                break;
            case "500":
                popUp("Internal error");
                break;
            default:
                popUp("Error during modification");
                break;
        }
        finish();
    }

    private String convertParameters(Map <String,String> data){
        String result = "";
        for (Map.Entry<String,String> e: data.entrySet()){
            result += e.getKey()+"="+e.getValue()+"&";
        }
        return result.substring(0,result.length()-1);
    }

    public String postData() throws IOException {
        URL urlToRequest = new URL(request);
        HttpURLConnection urlConnection = null;
        String postParameters = convertParameters(data);
        Log.d(ResponseHandler.class.toString(), postParameters);
        urlConnection = (HttpURLConnection) urlToRequest.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type",
                "application/x-www-form-urlencoded");
        urlConnection.setFixedLengthStreamingMode(postParameters.getBytes().length);
        PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
        out.print(postParameters);
        out.close();
        return urlConnection.getResponseMessage();
    }

    private void deleteData() throws MalformedURLException, IOException {
        Log.i("Request:",request);
        URL page = new URL(request);
        HttpURLConnection conn = null;
        conn = (HttpURLConnection) page.openConnection();
        conn.connect();
        InputStreamReader in = null;
        String message = conn.getResponseMessage();
        return;
    }
}
