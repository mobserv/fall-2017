package redfoxes.achievements.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by paulj on 11/12/2017.
 */

public class Profil implements Serializable {

    private String id;
    private int points;
    private String name;
    private ArrayList<ProfilSuccess> success;
    private ArrayList<Achievement> wanted;
    private String pict;
    private boolean modo;

    public Profil(String id, int points, String name, ArrayList<ProfilSuccess> success, ArrayList<Achievement> wanted,Bitmap pict, boolean modo) {
        this.id = id;
        this.points = points;
        this.name = name;
        this.success = success;
        setBitmapPict(pict);
        this.modo = modo;
        this.wanted = wanted;
    }

    public String getId() {
        return id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public ArrayList<ProfilSuccess> getSuccess() {
        return success;
    }

    public Bitmap getBitmapPict() {
        byte[] byteArrayBitmap = Base64.decode(pict, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(byteArrayBitmap, 0, byteArrayBitmap.length);
    }

    public boolean isModo() {
        return modo;
    }

    public Bitmap getCirclePict() {
        Bitmap bitmapimg = getBitmapPict();
        Bitmap output = Bitmap.createBitmap(bitmapimg.getWidth(),
                bitmapimg.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmapimg.getWidth(),
                bitmapimg.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmapimg.getWidth() / 2,
                bitmapimg.getHeight() / 2, bitmapimg.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmapimg, rect, rect, paint);
        return output;
    }

    public void setBitmapPict(Bitmap pict) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        pict.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayBitmap = baos.toByteArray();
        this.pict= Base64.encodeToString(byteArrayBitmap, Base64.DEFAULT);
    }

    public String getPict(){
        return pict;
    }
}
