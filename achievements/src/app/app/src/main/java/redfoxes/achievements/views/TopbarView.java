package redfoxes.achievements.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redfoxes.achievements.R;
import redfoxes.achievements.activities.ProfilActivity;
import redfoxes.achievements.activities.ScoreActivity;
import redfoxes.achievements.activities.Settings;
import redfoxes.achievements.utils.Achievement;
import redfoxes.achievements.utils.Profil;
import redfoxes.achievements.utils.ProfilSuccess;

/**
 * Created by paulj on 17/11/2017.
 */

public class TopbarView extends LinearLayout {
    private AppCompatActivity context;
    private Profil profil;

    public TopbarView(final AppCompatActivity context, Boolean back) {
        super(context);
        this.context = context;
        ArrayList<ProfilSuccess> success = null;
        Bitmap bm = BitmapFactory.decodeResource(getResources(),R.mipmap.marine);
        ArrayList<Achievement> wanted = null;
        profil = new Profil("idProfil",12600,"Marine",success,wanted,bm,true);//TODO get profil from preferences
        init(back);
    }

    private void init(Boolean back) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.from(context).inflate(R.layout.topbar, this, true);

        TextView nameTextView = (TextView) findViewById(R.id.name_textView__topbar);
        nameTextView.setText(profil.getName());
        TextView scoreTextView = (TextView) findViewById(R.id.score_textView__topbar);
        scoreTextView.setText(String.valueOf(profil.getPoints())+" points");

        ImageView profilImage = (ImageView) findViewById(R.id.profilPhoto_imageView__topbar);
        if(!context.getClass().equals(ProfilActivity.class)) {
            profilImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ProfilActivity.class).putExtra("profil",profil));
                }
            });
            nameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ProfilActivity.class).putExtra("profil",profil));
                }
            });
        }
        profilImage.setImageBitmap(profil.getCirclePict());
        ImageView preferencesImage = (ImageView) findViewById(R.id.gear_imageView__topbar);
        preferencesImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Settings.class));
            }
        });
        ImageView backbutton = (ImageView) findViewById(R.id.back_imageView__topbar);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.finish();
            }
        });

        if (!back) {
            backbutton.setVisibility(INVISIBLE);
        }

    }

    public TopbarView(final AppCompatActivity context) {
        super(context);
        this.context = context;
        init(false);
    }
}
