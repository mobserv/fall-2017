package redfoxes.achievements.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.Achievement;
import redfoxes.achievements.utils.Profil;
import redfoxes.achievements.utils.ProfilScoreAdapter;

public class Success extends AppCompatActivity {
    private Achievement achiev;
    private TextView achiev_title;
    private TextView achiev_points;
    private EditText achiev_date;
    private ImageView achiev_pict;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        Intent intent= getIntent();
        achiev = (Achievement) intent.getSerializableExtra("achievement");
        fillForm();
    }

    private void fillForm(){
        achiev_title = (TextView) findViewById(R.id.title_textView__success);
        achiev_points = (TextView) findViewById(R.id.points_textView__success);
        achiev_date = (EditText) findViewById(R.id.date_editText__success);
        achiev_pict = (ImageView) findViewById(R.id.picture_imageView__success);

        achiev_title.setText(achiev.getTitle());
        String str_points = Integer.toString(achiev.getPoints())+" points";
        achiev_points.setText(str_points);
        achiev_pict.setImageBitmap(achiev.getPictBitmap());

        achiev_date.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    public void validate(View v){
        EditText com = (EditText) findViewById(R.id.com_editText__success);
        String comment = String.valueOf(com.getText());
        finish();
    }

    public void back(View v){
        finish();
    }
}