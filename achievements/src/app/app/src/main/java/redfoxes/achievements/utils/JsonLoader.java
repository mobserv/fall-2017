package redfoxes.achievements.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;

public class JsonLoader extends AsyncTaskLoader<List<Achievement>> {

    public JsonLoader(Context context){
        super(context);
        Log.i("main","loader created");
    }

    public static String loadImage(String str_url){
        Bitmap image = null;
        String str_image;
        try {
            URL url = new URL(str_url);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(IOException e) {
            System.out.println(e);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayBitmap = baos.toByteArray();
        str_image=Base64.encodeToString(byteArrayBitmap, Base64.DEFAULT);
        return str_image;
    }

    @Override
    public List<Achievement> loadInBackground() {
        Log.i("main", "loader in Background");
        URL page = null;
        try {
            page = new URL("http://1-dot-newachievements-189013.appspot.com/achievementslist?respType=json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        StringBuffer text = new StringBuffer();
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) page.openConnection();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            conn.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStreamReader in = null;
        String jsonString = null;
        try {
            in = new InputStreamReader((InputStream) conn.getContent(), "UTF-8");
            Reader reader = in;
            jsonString = readAll(reader);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (jsonString != null) {
            List<Achievement> result;
            try {
                JSONArray listOfAchiev = new JSONArray(jsonString);
                int len = listOfAchiev.length();
                result = new ArrayList<Achievement>(len);
                Log.i("main", "json string length is " + len);
                for (int i = 0; i < len; i++) {
                    result.add(new Achievement(listOfAchiev.getJSONObject(i)));
                }
            } catch (JSONException e) {
                //TODO handle
                e.printStackTrace();
                return null;


            }
            return result;

        } else {

            return null;
        }
    }

    private String readAll(Reader reader) throws IOException {
        StringBuilder builder = new StringBuilder(4096);
        for (CharBuffer buf = CharBuffer.allocate(512); (reader.read(buf)) > -1; buf
                .clear()) {
            builder.append(buf.flip());
        }
        return builder.toString();
    }
}

