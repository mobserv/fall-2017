package redfoxes.achievements.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import redfoxes.achievements.R;

/**
 * Created by paulj on 16/12/2017.
 */

public class ProfilScoreAdapter extends ArrayAdapter<Profil> {

    private final Context context;
    private final int id;
    private final List<Profil> profils;
    private Profil profil;

    public ProfilScoreAdapter(Context context, int id, List<Profil> profils)
    {
        super(context,id, profils);
        this.context = context;
        this.id = id;
        this.profils = profils;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(id, parent, false);

        profil = profils.get(position);
        TextView profilName= (TextView) rowView.findViewById(R.id.name_textView__profil_score);
        TextView profilPoint= (TextView) rowView.findViewById(R.id.points_textView__profil_score);
        ImageView profilPict= (ImageView) rowView.findViewById(R.id.picture_imageView__profil_score);

        profilName.setText(profil.getName());
        profilPoint.setText(profil.getPoints()+" points");
        profilPict.setImageBitmap(profil.getBitmapPict());

        return rowView;
    }

}

