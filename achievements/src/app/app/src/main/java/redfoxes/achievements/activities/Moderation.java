package redfoxes.achievements.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.Achievement;
//import redfoxes.achievements.utils.ModeratorJsonLoader;
import redfoxes.achievements.utils.AnswerServer;
import redfoxes.achievements.views.TagView;

public class Moderation extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Achievement>> {
    private RatingBar points;
    private TextView title;
    private ArrayList<TagView> arrayTagViews = new ArrayList<TagView>();
    private LinearLayout tagsLinear;
    private ImageView picture;
    private List <Achievement> modoList;
    private Achievement modo_achiev;
    private int nbOfFake;
    // url http://lh3.googleusercontent.com/APReYAcwWlMcnTYm8zJgaolTc8lRsNfYq02wVec8WGB6xsvwIJaI-3LZR59xou-9ka1VnfwlwQ=w128-h128-n-no-v1

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moderation);
        nbOfFake=getIntent().getIntExtra("ach",0);
        title = (TextView) findViewById(R.id.title_editText__moderation);
        picture = (ImageView) findViewById(R.id.picture_imageView__moderation);
        points = (RatingBar) findViewById(R.id.points_ratingBar__moderation);
        tagsLinear = (LinearLayout) findViewById(R.id.tags_LinearLayout__moderation);
        getAchievMod();
        initTags();
    }


    public void accept(View view) {
        startActivity(new Intent(this, Moderation.class));
        finish();
    }

    public void refuse(View view) {
        startActivity(new Intent(this, Moderation.class));
        finish();
    }

    public void back(View view) {
        finish();
    }

    //get a new achievement for moderation
    public void getAchievMod(){
        if(nbOfFake==0) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.boat_img);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add("travel");
            tags.add("work");
            modo_achiev = new Achievement("100", "Buy a boat", 100, bitmap, tags);
        }else {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.cards);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add("sport");
            tags.add("game");
            tags.add("luck");
            modo_achiev = new Achievement("100", "Win a Poker tournament", 100, bitmap, tags);
        }

        picture.setImageBitmap(modo_achiev.getPictBitmap());
        title.setText(modo_achiev.getTitle());

    }

    /**
     * Get the selected tags
     * @return the ArrayList of String of the selected tags
     * @see ArrayList
     */
    private ArrayList<String> getSelectedTag(){
        ArrayList<String> tagsStringArray = new ArrayList<String>();
        for (TagView tag : arrayTagViews){
            if(tag.isSelected()){
                tagsStringArray.add(tag.getName());
            }
        }
        return tagsStringArray;
    }

    /**
     * Init the tags list and tagsView list with the ArrayList from the Achievement of the intent
     */
    private void initTags() {
        String[] tagsStringArray = getResources().getStringArray(R.array.list_tags);
        ArrayList<String> tagsOfAch = modo_achiev.getTags();
        for (String tagName : tagsStringArray){
            boolean iSselected = tagsOfAch.contains(tagName);
            TagView nTagView = new TagView(this,tagName,iSselected,true);
            arrayTagViews.add(nTagView);
            tagsLinear.addView(nTagView);
        }
    }

    public void validate(View view){
        validate();
    }

    public void validate(){
        modo_achiev.setTitle(String.valueOf(title.getText()));
        modo_achiev.setTags(getSelectedTag());
        modo_achiev.setPoints(50+ (int)(points.getRating()*500));
        Log.i("Send Modification",String.valueOf(modo_achiev.getPoints()));
        if(modo_achiev.getTitle()==""){
            Toast.makeText(this, getString(R.string.giveATitle), Toast.LENGTH_SHORT).show();
        }else {
            if (modo_achiev.getTags().size() == 0) {
                Toast.makeText(this, getString(R.string.selectOneTag), Toast.LENGTH_SHORT).show();
            } else {
                //TODO send modification
                sendModification();
                openNewModeration();
            }
        }
    }

    public void sendModification() {
        String title = String.valueOf(modo_achiev.getTitle());
        String pict = "";
        String points = String.valueOf(modo_achiev.getPoints());
        ArrayList<String> achieTags = modo_achiev.getTags();
        Handler handler = new Handler();
        Iterator it = achieTags.iterator();
        String achiev_tags = "";
        int N_tags = achieTags.size();
        int i = 1;
        while (it.hasNext()) {
            Object tag = it.next();
            if (i < N_tags) {
                achiev_tags += tag + ",";
            } else {
                achiev_tags += tag;
            }
            i += 1;
        }
        Map<String, String> data = new HashMap<String, String>();
        if (title.equals("")) {
            Toast.makeText(this, getString(R.string.giveATitle), Toast.LENGTH_SHORT).show();
        } else {
            if (achieTags.size() == 0) {
                Toast.makeText(this, getString(R.string.selectOneTag), Toast.LENGTH_SHORT).show();
            } else {
                if (pict.equals("")) {
                    data.put("tags", achiev_tags);
                    if(nbOfFake==0) {
                        data.put("pict", "https://www.nobleboatsinternational.com/wp-content/uploads/2016/03/6.85m-Thumb.jpg");
                    }else{
                        data.put("pict", "http://www.pokerwindowsphone.com/images/logo.png");
                    }
                    data.put("title", title);
                    data.put("points", points);
                    Log.i("Submission", "Image" + pict);
                    ResponseHandler rh = new ResponseHandler(this, "add", "POST", handler, this, data);
                    new AnswerServer().execute(rh);
                    finish();
                } else {
                    data.put("tags", achiev_tags);
                    data.put("pict", pict);
                    data.put("title", title);
                    data.put("points", points);
                    Log.i("Submission", "Image" + pict);
                    ResponseHandler rh = new ResponseHandler(this, "add", "POST", handler, this, data);
                    new AnswerServer().execute(rh);
                    finish();
                }
            }
        }
    }

    public void reject(View view){
        reject();
    }

    public void reject(){
        //TODO send delete
        openNewModeration();
    }

    public void openNewModeration(){
        Intent intent = new Intent(this, Moderation.class);
        intent.putExtra("ach",1);
        startActivity(intent);
        finish();
    }

    @Override
    public Loader<List<Achievement>> onCreateLoader(int i, Bundle bundle) {
        Log.i("moderation", "creating loader Mod");
        //ModeratorJsonLoader modoloader = new ModeratorJsonLoader(this);
        //modoloader.forceLoad();
        //return modoloader;
        return null;
    }

    @Override
    public void onLoadFinished(Loader<List<Achievement>> modoloader, List<Achievement> ModoAchievements) {
        modoList = ModoAchievements;
        modo_achiev = modoList.get(0);
        title.setText(modo_achiev.getTitle());
        picture.setImageBitmap(modo_achiev.getPictBitmap());
        String tagName;
        Iterator i = modo_achiev.getTags().iterator();
        while(i.hasNext()){
            tagName = (String)i.next();
            TagView nTag = new TagView(this,tagName,false,false);
            arrayTagViews.get(arrayTagViews.indexOf(nTag)).isSelected();
        }
        getSelectedTag();
    }

    @Override
    public void onLoaderReset(Loader<List<Achievement>> loader) {

    }
}