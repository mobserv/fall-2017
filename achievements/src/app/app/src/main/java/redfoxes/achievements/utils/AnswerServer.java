package redfoxes.achievements.utils;

import android.os.AsyncTask;

import redfoxes.achievements.activities.ResponseHandler;

public class AnswerServer extends AsyncTask<ResponseHandler, Void, String> {
    private ResponseHandler rh;

    @Override
    protected String doInBackground(ResponseHandler... rhs) {
        rh = rhs[0];
        return rh.start();
    }
    @Override
    protected void onPostExecute(String response) {
        rh.respond(response);
    }
}