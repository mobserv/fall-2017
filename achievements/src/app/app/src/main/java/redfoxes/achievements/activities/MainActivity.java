package redfoxes.achievements.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Spinner;

import java.io.Serializable;
import java.util.ArrayList;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.Achievement;
import redfoxes.achievements.utils.SimpleFragmentPagerAdapter;
import redfoxes.achievements.views.TopbarView;


/**
 * Created by paulj on 03/12/2017.
 */

public class MainActivity extends AppCompatActivity{
    private Spinner periods;
    private ViewPager viewPager;
    private Achievement Achi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the  activity_main.xml layout file
        setContentView(R.layout.activity_main);

        // Find the view pager that will allow the user to swipe between fragments
        viewPager = (ViewPager) findViewById(R.id.viewpager__main);

        // Create an adapter that knows which fragment should be shown on each page
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);

        // Set the topBar
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.topbarslot_FrameLayout__main);
        frameLayout.addView(new TopbarView(this, false));

    }

    @Override
    public void onBackPressed() {
        if(viewPager.getCurrentItem()==1){
            finish();
        }else{
            viewPager.setCurrentItem(1);
        }
    }

    public void openSubmission(View view){
        startActivity(new Intent(this, Submission.class));
    }

    public void openModeration(View v){
        Intent intent = new Intent(this, Moderation.class);
        intent.putExtra("ach",0);
        startActivity(intent);
    }

    public void openSuccess(View v){
        Intent intent = new Intent(this, Success.class);
        ArrayList<String> tags = new ArrayList<String>();
        tags.add("sport");
        tags.add("art");
        tags.add("X");
        Bitmap bt = BitmapFactory.decodeResource(this.getResources(),R.mipmap.ic_achievement_logo);
        Achievement achiev = new Achievement("idA","TitleA",1000,bt,tags);
        intent.putExtra("achiev",achiev);
        startActivity(intent);
    }

}
