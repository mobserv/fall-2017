package redfoxes.achievements.utils;

/**
 * Created by Pauline on 05/01/2018.
 */

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;

public class ModeratorJsonLoader extends AsyncTaskLoader<List<Achievement>> {

    public ModeratorJsonLoader(Context context){
        super(context);
        Log.i("moderation","loader created");
    }

    @Override
    public List<Achievement> loadInBackground() {
        Log.i("moderation", "loader in Background");
        URL page = null;
        try {
            page = new URL("http://1-dot-newachievements-189013.appspot.com/modolist?respType=json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        StringBuffer text = new StringBuffer();
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) page.openConnection();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            conn.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStreamReader in = null;
        String jsonString = null;
        try {
            in = new InputStreamReader((InputStream) conn.getContent(), "UTF-8");
            Reader reader = in;
            jsonString = readAll(reader);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (jsonString != null) {
            List<Achievement> result;
            try {
                JSONArray listOfAchiev = new JSONArray(jsonString);
                int len = listOfAchiev.length();
                result = new ArrayList<Achievement>(len);
                Log.i("moderation", "json string length is " + len);
                for (int i = 0; i < len; i++) {
                    result.add(new Achievement(listOfAchiev.getJSONObject(i)));
                }
            } catch (JSONException e) {
                //TODO handle
                e.printStackTrace();
                return null;


            }
            return result;

        } else {

            return null;
        }
    }

    private String readAll(Reader reader) throws IOException {
        StringBuilder builder = new StringBuilder(4096);
        for (CharBuffer buf = CharBuffer.allocate(512); (reader.read(buf)) > -1; buf
                .clear()) {
            builder.append(buf.flip());
        }
        return builder.toString();
    }
}
