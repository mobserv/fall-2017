package redfoxes.achievements.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;

import redfoxes.achievements.R;

/**
 * Created by paulj on 11/12/2017.
 */

public class Achievement implements Serializable {

    private String id;
    private String title;
    private int points;
    private String pict = null;
    private ArrayList<String> tags;
    private boolean hide = false;
    private boolean done = false;
    private boolean signaled = false;
    private boolean later = false;

    public Achievement(String id, String title, int points, Bitmap bitmap, ArrayList<String> tags) {
        this.id = id;
        this.title = title;
        this.points = points;
        setPictBitmap(bitmap);
        this.tags = tags;
    }

    public Achievement(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getString("id");
        title = jsonObject.getString("title");
        points = Integer.parseInt(jsonObject.getString("points"));
        pict = jsonObject.getString("pict");
        try {
            URL url = new URL(pict);
            Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            setPictBitmap(bitmap);
            Log.i("url",title+" ___ "+getPict());
        } catch(Exception e) {
            System.out.println(e);
            Log.i("base64",title+" ___ "+getPict());
        }
        String[] listtags = jsonObject.getString("tags").split(",");
        tags = new ArrayList<String>();
        for(int i=0;i<listtags.length;i++){
            tags.add(i,listtags[i]);
        }
    }

    public Achievement(String title, int points, ArrayList<String> tags) {
        this.title = title;
        this.points = points;
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title=title;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setTags(ArrayList<String> tags){ this.tags=tags;}

    public Bitmap getPictBitmap() {
        Bitmap bt = BitmapFactory.decodeResource(Resources.getSystem(), R.mipmap.ic_achievement_logo);
        try {
            byte[] byteArrayBitmap = Base64.decode(pict, Base64.DEFAULT);
            bt = BitmapFactory.decodeByteArray(byteArrayBitmap, 0, byteArrayBitmap.length);
        }catch (Exception e){
        }
        return bt;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setPictBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayBitmap = baos.toByteArray();
        this.pict=Base64.encodeToString(byteArrayBitmap, Base64.DEFAULT);
    }

    public String getPict(){
        return pict;
    }


    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }


    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isSignaled() {
        return signaled;
    }

    public void setSignaled(boolean signaled) {
        this.signaled = signaled;
    }

    public boolean isLater() {
        return later;
    }

    public void setLater(boolean later) {
        this.later = later;
    }
}
