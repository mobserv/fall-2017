package redfoxes.achievements.activities;

import android.app.LoaderManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.Profil;
import redfoxes.achievements.utils.ProfilScoreAdapter;

import static android.support.v7.appcompat.R.id.screen;

public class ScoreActivity extends Fragment {//implements LoaderManager.LoaderCallbacks<List<Profil>>, AdapterView.OnItemClickListener {

    private Spinner periods;
    private int maxMarginPic;
    private ImageView pic;
    private List<Profil> profils;
    private ListView listView;

    public ScoreActivity() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.activity_score, container, false);

        periods = (Spinner) view.findViewById(R.id.period_spinner__score);
        List periodList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.list_periods)));
        ArrayAdapter periodsAdapter = new ArrayAdapter(
                getContext(),
                android.R.layout.simple_spinner_item,
                periodList
        );
        periodsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        periods.setAdapter(periodsAdapter);

        listView = (ListView) view.findViewById(R.id.scores_listView__score);

        pic = (ImageView) view.findViewById(R.id.pic_imageView__score);
        final FrameLayout bar = (FrameLayout) view.findViewById(R.id.bar_frameLayout__score);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    FrameLayout bar = (FrameLayout) view.findViewById(R.id.bar_frameLayout__score);
                    maxMarginPic = Math.max(pxToDp(bar.getHeight())-25,0);
                    Log.i("taille", String.valueOf(maxMarginPic));
                    upPic(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return view;
    }

    public void init() {

    }

    public void upPic(int bot){
        //bot entre 0 et maxMarginPic
        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(pic.getLayoutParams());
        marginParams.setMargins(dpToPx(100),dpToPx(maxMarginPic-bot+40),0,0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
        pic.setLayoutParams(layoutParams);
    }

    public int dpToPx(int dp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }
    public int pxToDp(int px){
        return (int) (px / ((float) getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    /*
    @Override
    public android.content.Loader<List<Profil>> onCreateLoader(int id, Bundle args) {
        Log.i("main", "creating loader");
        return null;
        //JsonLoader loader = new JsonLoader(this);
        //loader.forceLoad();
        //return loader;
        //TODO faire le jsonLoaderProfils
    }

    @Override
    public void onLoadFinished(android.content.Loader<List<Profil>> arg0, List<Profil> profils) {
        ProfilScoreAdapter Ca;
        Ca = new ProfilScoreAdapter(getContext(),R.layout.profil_score_view, profils);
        listView.setAdapter(Ca);
    }

    @Override
    public void onLoaderReset(android.content.Loader<List<Profil>> loader) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Log.i("Main:","someone clicked on item: "+position);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i("main", "onResume");
        //getLoaderManager().restartLoader(0,null,this);
    }
    */
    //TODO load les profils
}
