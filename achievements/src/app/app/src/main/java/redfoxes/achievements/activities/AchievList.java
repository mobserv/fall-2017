package redfoxes.achievements.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.Achievement;
import redfoxes.achievements.utils.AchievementAdapter;
import redfoxes.achievements.utils.JsonLoader;
import redfoxes.achievements.views.TagView;

public class AchievList extends Fragment implements LoaderManager.LoaderCallbacks<List<Achievement>>{

    private ArrayList<TagView> arrayTagViews = new ArrayList<TagView>();
    private LinearLayout tagsLinear_AchievList;
    private ListView listView;
    private List <Achievement> achievementList = new ArrayList<Achievement>();
    private ArrayList<String> selectedTags;
    private ArrayList<String> filterText= new ArrayList<String>();
    public AchievList() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_achiev_list, container, false);
        tagsLinear_AchievList = (LinearLayout) view.findViewById(R.id.tags_LinearLayout__achiev_list);
        initTags();
        listView = (ListView) view.findViewById(R.id.list_achiev);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView parentView, View childView,int position,long id){
                Log.i("Main:","someone clicked on item: "+position);
            }
        });
        final EditText filterEditText = (EditText) view.findViewById(R.id.shearch_editText__achiev_list);
        filterEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterText=new ArrayList<String>(new ArrayList<String>(Arrays.asList(filterEditText.getText().toString().split(" "))));
                while(filterText.remove("")){}
                actualizefilteredList();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    /**
     * Get the selected tags
     * Actualize the ArrayList of String of the selected tags
     * @see TagView
     * @see ArrayList
     */
    private void getSelectedTag(){
        ArrayList<String> tagsStringArray = new ArrayList<String>();
        for (TagView tagView : arrayTagViews){
            if(tagView.isSelected()){
                tagsStringArray.add(tagView.getName());
            }
        }
        selectedTags = tagsStringArray;
    }

    /**
     * Init the tags list and tagsView list with the ArrayList from string.xml
     */
    private void initTags() {
        String[] tagsStringArray = getResources().getStringArray(R.array.list_tags);
        for (String tagName : tagsStringArray){
            final TagView nTagView = new TagView(getContext(),tagName,false,true);
            nTagView.getImageTag().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("Tag","Click on"+nTagView.getName());
                    nTagView.clickOnTag();
                    actualizefilteredList();
                }
            });
            arrayTagViews.add(nTagView);
            tagsLinear_AchievList.addView(nTagView);
        }
    }

    public void actualizefilteredList() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean hideAlreadyAchieved = prefs.getBoolean("hideAlreadyAchieved", false);
        boolean hideSignaled = prefs.getBoolean("hideSignaled", true);
        boolean hideChooseToHide = prefs.getBoolean("hideChooseToHide", true);
        getSelectedTag();
        List<Achievement> newAchievementsList = new ArrayList<>(achievementList);
        if(!selectedTags.isEmpty()){
            ArrayList<Achievement> newTagedAchievementsList = new ArrayList<>();
            for (Achievement achievement: newAchievementsList){
                ArrayList<String> achievTags = new ArrayList<>(achievement.getTags());
                if(achievTags.containsAll(selectedTags)) {
                    newTagedAchievementsList.add(achievement);
                }
            }
            newAchievementsList = newTagedAchievementsList;
        }
        if(filterText.size()>0){
            ArrayList<Achievement> newStringFilteredAchievementsList = new ArrayList<>();
            for (Achievement achievement: newAchievementsList){
                String title=achievement.getTitle();
                if(isListStingInString(filterText,title)){
                    newStringFilteredAchievementsList.add(achievement);
                }
            }
            newAchievementsList = newStringFilteredAchievementsList;
        }
        ArrayList<Achievement> newStringFilteredAchievementsList = new ArrayList<>();
        for (Achievement achievement: newAchievementsList){
            String title=achievement.getTitle();
            if (!((achievement.isDone() && hideAlreadyAchieved) || (achievement.isHide() && hideChooseToHide) || (achievement.isSignaled() && hideSignaled))){
                newStringFilteredAchievementsList.add(achievement);
            }
        }
        newAchievementsList = newStringFilteredAchievementsList;
        actualizeList(newAchievementsList);

    }

    private boolean isListStingInString(ArrayList<String> filterText, String title) {
        for( String mot : filterText){
            if(!title.toLowerCase().contains(mot.toLowerCase())){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i("main", "onResume");
        getLoaderManager().restartLoader(0,null,this);
    }

    @Override
    public Loader<List<Achievement>> onCreateLoader(int i, Bundle bundle) {
        Log.i("main", "creating loader");
        JsonLoader loader = new JsonLoader(this.getActivity());
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Achievement>> loader, List<Achievement> achievements) {
        achievementList = achievements;
        AchievementAdapter Aa;
        Aa = new AchievementAdapter(this.getActivity(),R.layout.ach_list_item_view, achievementList, this);
        listView.setAdapter(Aa);
    }

    @Override
    public void onLoaderReset(Loader<List<Achievement>> loader) {

    }

    public void actualizeList(List<Achievement> achievements) {
        AchievementAdapter Aa = new AchievementAdapter(this.getActivity(), R.layout.ach_list_item_view, achievements, this);
        listView.setAdapter(Aa);
    }
}
