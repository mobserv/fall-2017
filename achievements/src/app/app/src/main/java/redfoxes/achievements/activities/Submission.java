package redfoxes.achievements.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import redfoxes.achievements.R;
import redfoxes.achievements.utils.AnswerServer;
import redfoxes.achievements.views.TagView;

/**
 *  Activity class of submission
 */
public class Submission extends AppCompatActivity {
    private LinearLayout tagsLinear;
    private EditText titleEditText;
    private ImageView pictImageView;
    private String pictSt;
    private ArrayList<TagView> arrayTagViews = new ArrayList<TagView>();
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);

        tagsLinear = (LinearLayout) findViewById(R.id.tags_LinearLayout__submission);
        titleEditText = (EditText) findViewById(R.id.title_editText__moderation);
        pictImageView = (ImageView) findViewById(R.id.picture_imageView__submission);

        initTags();
        setPictSt(BitmapFactory.decodeResource(getResources(),R.drawable.ic_achievement_logo));
    }

    /**
     * Init the tags list and tagsView list with the ArrayList from string.xml
     */
    private void initTags() {
        String[] tagsStringArray = getResources().getStringArray(R.array.list_tags);
        for (String tagName : tagsStringArray){
            TagView nTagView = new TagView(this,tagName,false,true);
            arrayTagViews.add(nTagView);
            tagsLinear.addView(nTagView);
        }
    }

    /**
     * Close the submission without submit it
     * Call by the back ImageView of the layout
     * @param view the ImageViewButton
     */
    public void back(View view) {
        finish();
    }

    /**
     * Send the request
     * Call by the ImageView of the layout
     * @param view the ImageViewButton
     */
    public void validate(View view) {
        String title = String.valueOf(titleEditText.getText());
        String pict = "";
        String points = "-1";
        ArrayList<String> achieTags = getSelectedTag();
        Handler handler= new Handler();
        Iterator it = achieTags.iterator();
        String achiev_tags = "";
        int N_tags = achieTags.size();
        int i = 1;
        while(it.hasNext()){
            Object tag = it.next();
            if(i<N_tags) {
                achiev_tags += tag + ",";
            }else{
                achiev_tags += tag;
            }
            i += 1;
        }
        Map<String,String> data = new HashMap<String,String>();
        Log.i("Send Submission",pictSt);
        if(title.equals("")){
            Toast.makeText(this, getString(R.string.giveATitle), Toast.LENGTH_SHORT).show();
        }else {
            if (achieTags.size() == 0) {
                Toast.makeText(this, getString(R.string.selectOneTag), Toast.LENGTH_SHORT).show();
            } else {
                if (pict.equals("")) {
                    data.put("tags" , achiev_tags);
                    data.put("pict" , "https://fr.cdn.v5.futura-sciences.com/buildsv6/images/wide1920/d/e/1/de1aa8e9b3_96048_renard-domestication-chien.jpg");
                    data.put("title" , title);
                    data.put("points" , points);
                    Log.i("Submission" , "Image" + pict);
                    ResponseHandler rh = new ResponseHandler(this, "add", "POST" , handler, this, data);
                    new AnswerServer().execute(rh);
                    finish();
                } else {
                    data.put("tags" , achiev_tags);
                    data.put("pict" , pict);
                    data.put("title" , title);
                    data.put("points" , points);
                    Log.i("Submission" , "Image" + pict);
                    ResponseHandler rh = new ResponseHandler(this, "add", "POST" , handler, this, data);
                    new AnswerServer().execute(rh);
                    finish();
                }
            }
        }
    }

    /**
     * Get the selected tags
     * @return the ArrayList of String of the selected tags
     * @see TagView
     * @see ArrayList
     */
    private ArrayList<String> getSelectedTag(){
        ArrayList<String> tagsStringArray = new ArrayList<String>();
        for (TagView tagView : arrayTagViews){
            if(tagView.isSelected()){
                tagsStringArray.add(tagView.getName());
            }
        }
        return tagsStringArray;
    }

    /**
     * Start dialog for adding picture
     * Can get it from libraryor camera
     * @param view the imageView it self
     */
    public void getPicture(View view){
        final CharSequence[] items = { "Take Photo", "Choose from Library","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Send intent to the camera
     */
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 10);
            return;
        }
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    /**
     * Send intent to the library
     */
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    /**
     * Convert Bitmap to string by byteArray base64 convertion
     * @param bitmap the image
     * @return
     */
    public String fromBitmaptostring(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayBitmap = baos.toByteArray();
        return Base64.encodeToString(byteArrayBitmap, Base64.DEFAULT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        setPictSt(bm);
    }

    /**
     * Save the picture took by the camera
     * @param data From camera
     */
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setPictSt(thumbnail);
    }

    public void setPictSt(Bitmap bm) {
        pictSt=fromBitmaptostring(bm);
        pictImageView.setImageBitmap(bm);
    }

}
