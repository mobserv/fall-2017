Achievements - Achieve them all!
Platform: Android
Short description: Achievements is an application where you can show what you have achieve in your life. But you are not alone, Achievements is also a community! Compare yourself with your friends and see who is the best! You have already swim with shark or just see all the Harry Potter saga in one night? You want to go around the world? Share it! 
Implemented features: Creation, Moderation and search of an Achievements
Used frameworks: handler, Achievement, Tags, Fragments, Profil, sharePrefferences
Required permission: 	CAMERA
			INTERNET
			READ_EXTERNAL_STORAGE
			WRITE_EXTERNAL_STORAGE