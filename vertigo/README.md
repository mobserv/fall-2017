- Name and slogan 
    name : Vertigo
    slogan : the rock climbing spirit

- platform: Android

- short description of this project
Vertigo is a Free Rock Climbers application who allow to check your performance via generation of
statistics (chart) and check also the performance of your partners and compare them.
Your data can be provide by two Content Providers
 	- from www.8a.nu � Existing climber have their tick list stored there
 	- VertigoDB � our own DB on Google Cloud Platform (GAE)

- implemented features 
chart of climber's scorecards 
Ranking climber's ascent
give statistics on climber's performance
Team Concept � Climbing Partners (add one partners in preference)
Performance comparison between climbing partners (statistics, charts)

- used frameworks
Navigation Drawers 
View Pager
MPAndroidChart
Picasso
Dynamic TableLayout
FragmentActivity
Expandable ListView
FireBase RealTime Database
Cloud Google (GAE)


- required permissions 
INTERNET
ACCESS_NETWORK_STATE
