package mobile.services.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.services.facilities.OperationResult;
import mobile.services.providers.vertigo.VertigoDB;

@SuppressWarnings("serial")
public class AuthenticationServlet extends HttpServlet {
	
	private static final Logger logger = Logger.getLogger(AuthenticationServlet.class.getName());
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		OperationResult operationResult = VertigoDB.getInstance().fetchUserData(request);
		logger.log(Level.INFO, operationResult.toString());
		
		response.setContentType("application/json; charset=UTF-8");
		response.getWriter().println(operationResult.toJsonString());
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String path = request.getServletPath();
		
		OperationResult operationResult ;
		if ( path.contains("/user/register" ) )
		{
			operationResult = VertigoDB.getInstance().performUserRegistration(request);
		}
		else
		{
			operationResult = VertigoDB.getInstance().performUserLogon(request);
		}
		
		logger.log(Level.INFO, operationResult.toString());
		
		response.setContentType("application/json; charset=UTF-8");
		response.getWriter().println(operationResult.toJsonString());
	}

}
