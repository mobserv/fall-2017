package mobile.services.servlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import mobile.services.entities.EAscent;
import mobile.services.entities.EClimber;
import mobile.services.entities.ECountry;
import mobile.services.facilities.OperationResult;
import mobile.services.facilities.VertigoException;
import mobile.services.model.Ascent;
import mobile.services.model.Climber;
import mobile.services.providers.h8a.H8aFacade;
import mobile.services.providers.vertigo.VertigoDB;

@SuppressWarnings("serial")
public class AscentServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(AscentServlet.class.getName());
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String xID = request.getParameter("ID");
		if ( xID==null) xID = request.getParameter("xID");
		String mode = request.getParameter("M");
		
		OperationResult operationResult ;
		String path = request.getServletPath();
		if ( path.contains("/ascents/import" ) )
		{
			operationResult = H8aFacade.getInstance().fetchAscents(xID);
			operationResult = VertigoDB.getInstance().doImport(xID, operationResult);
		}
		else if ( path.contains("/notifications/clear" ) )
		{
			operationResult = VertigoDB.getInstance().clearNotifications(xID);
			operationResult.AddProperty("Mode", "Vertigo"); 
		}
		else if ( path.contains("/notifications" ) )
		{
			operationResult = VertigoDB.getInstance().fetchNotifications(xID);
			operationResult.AddProperty("Mode", "Vertigo"); 
		}
		else
		{
			if ( mode==null )  //Mode is not specified via the URL parameter
			{
				mode = VertigoDB.getInstance().getPreferredMode(xID);
			}
			
			if ( "V".equals(mode) )  //##CR - re-think that part ==> the mode should be implicit 
			{
				//Fetch Data from VertigoDB
				operationResult = VertigoDB.getInstance().fetchAscents(xID);
				operationResult.AddProperty("Mode", "Vertigo");  //##CR - re-think that part - Application Context 
			}
			else
			{
				//Fetch Data from 8a.nu
				operationResult = H8aFacade.getInstance().fetchAscents(xID);
				operationResult.AddProperty("Mode", "8a.nu"); //##CR - re-think that part - Application Context
			}
		}
		
		response.setContentType("application/json; charset=UTF-8");
		logger.log(Level.INFO, operationResult.toString());
		response.getWriter().println(operationResult.toJsonString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		//String path = request.getServletPath();
		
		OperationResult operationResult ;
		operationResult = VertigoDB.getInstance().storeNewAscent(request);
		
		logger.log(Level.INFO, operationResult.toString());
		
		response.setContentType("application/json; charset=UTF-8");
		response.getWriter().println(operationResult.toJsonString());
	}
}
