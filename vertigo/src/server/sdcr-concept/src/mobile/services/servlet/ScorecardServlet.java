package mobile.services.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.services.facilities.OperationResult;
import mobile.services.providers.h8a.H8aFacade;

@SuppressWarnings("serial")
public class ScorecardServlet extends HttpServlet {
	
	private static final Logger logger = Logger.getLogger(ScorecardServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String ID = req.getParameter("ID");
		if ( ID!=null )
		{
			resp.setContentType("application/json; charset=UTF-8");
	
			OperationResult operationResult = H8aFacade.getInstance().fetchData(ID);
			logger.log(Level.INFO, operationResult.toString());
			resp.getWriter().println(operationResult.toJsonString());
		}
		else
		{
			String userID = req.getParameter("UserID");
			if ( userID!=null )
			{
				resp.setContentType("application/json; charset=UTF-8");
				
				OperationResult operationResult = H8aFacade.getInstance().fetchHtmlData(userID);
				logger.log(Level.INFO, operationResult.toString());
				resp.getWriter().println(operationResult.toJsonString());
			}
		}
	}
}
