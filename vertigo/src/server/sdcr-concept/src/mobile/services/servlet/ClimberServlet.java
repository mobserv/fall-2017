package mobile.services.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.services.facilities.OperationResult;
import mobile.services.providers.h8a.H8aFacade;
import mobile.services.providers.vertigo.VertigoDB;

@SuppressWarnings("serial")
public class ClimberServlet extends HttpServlet {
	
	private static final Logger logger = Logger.getLogger(ClimberServlet.class.getName());
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String xid = request.getParameter("xid");
		String key = request.getParameter("key");
		
		response.setContentType("application/json; charset=UTF-8");
		OperationResult operationResult ;
		
		if ( xid!=null )
		{
			operationResult = H8aFacade.getInstance().fetchExternalClimberData(xid);
		}
		else if ( key!=null )
		{
			operationResult = VertigoDB.getInstance().fetchClimberData(key);
		}
		else
		{
			operationResult = VertigoDB.getInstance().fetchClimbers();
		}
		
		logger.log(Level.INFO, operationResult.toString());
		response.getWriter().println(operationResult.toJsonString());
	}
	
//	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		
//		OperationResult operationResult = VertigoDB.getInstance().performUserRegistration(request);
//		logger.log(Level.INFO, operationResult.toString());
//		response.getWriter().println(operationResult.toJsonString());
//	}

}
