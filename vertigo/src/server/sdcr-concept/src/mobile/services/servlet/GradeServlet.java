package mobile.services.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.services.facilities.JsonOperationResult;
import mobile.services.facilities.OperationResult;
import mobile.services.model.Grades;


@SuppressWarnings("serial")
public class GradeServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(GradeServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		resp.setContentType("application/json");
		
		String jsonObjectAsString = Grades.getInstance().getGradesAsJsonString();
		OperationResult operationResult = new JsonOperationResult(jsonObjectAsString);
		
		logger.log(Level.INFO, operationResult.toString());
		resp.getWriter().println(operationResult.toJsonString());
	}
}
