package mobile.services.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.services.facilities.OperationResult;
import mobile.services.providers.vertigo.VertigoDB;


@SuppressWarnings("serial")
public class CountryServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(CountryServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		resp.setContentType("application/json");
		OperationResult operationResult = VertigoDB.getInstance().fetchCountries();
		
		logger.log(Level.INFO, operationResult.toString());
		resp.getWriter().println(operationResult.toJsonString());
	}
}
