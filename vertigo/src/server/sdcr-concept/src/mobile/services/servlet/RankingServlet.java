package mobile.services.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.services.facilities.OperationResult;
import mobile.services.providers.h8a.H8aFacade;
import mobile.services.providers.vertigo.VertigoDB;

@SuppressWarnings("serial")
public class RankingServlet extends HttpServlet {
	
	private static final Logger logger = Logger.getLogger(RankingServlet.class.getName());
	
	public void doGet(HttpServletRequest request, HttpServletResponse resp) throws IOException {
		
		//String mode = request.getParameter("M");
		String IDs = request.getParameter("ID");
		if ( IDs==null )
		{
			// Comma separated list of ID(s)
			IDs = request.getParameter("IDs");
		}
		
		resp.setContentType("application/json; charset=UTF-8");
		OperationResult operationResult = VertigoDB.getInstance().fetchRanking(IDs);
		
//		if ( "V".equals(mode) )  //##CR - re-think that part ==> Abstract Factory Design Pattern required 
//		{
//			operationResult = VertigoDB.getInstance().fetchRanking(IDs);
//			operationResult.AddProperty("Mode", "Vertigo");  //##CR - re-think that part - Application Context 
//		}
//		else
//		{
//			operationResult = H8aFacade.getInstance().fetchRanking(IDs);
//			operationResult.AddProperty("Mode", "8a.nu");  //##CR - re-think that part - Application Context 
//		}
		
		logger.log(Level.INFO, operationResult.toString());
		resp.getWriter().println(operationResult.toJsonString());
	}
}
