package mobile.services.providers.h8a;

import java.util.ArrayList;

import com.google.appengine.labs.repackaged.org.json.*;
import mobile.services.facilities.*;
import mobile.services.model.Ascent;
import mobile.services.model.Climber;
import mobile.services.providers.IContentProvider;

public class H8aContentProvider implements IContentProvider
{
	private String rawData;
	private JSONObject jsonObject;
	private ArrayList<Ascent> ascents;
	private Climber climber;

	public H8aContentProvider initialize(String data) throws VertigoException 
	{
		this.rawData = data;
		this.parseRawData();
		
		return this; //Just to allow chaining
	}

//	public String getRawData() 
//	{
//		return this.rawData;
//	}

	@Override
	public Climber getClimber() throws VertigoException 
	{
		if ( this.climber==null )
		{
			this.climber = this._computeClimber();
		}
		return this.climber;
	}
	
	private Climber _computeClimber() throws VertigoException 
	{
		try 
		{
			JSONObject jsonObject = this.jsonObject.getJSONObject("user");
			Climber climber = new Climber().initialize(jsonObject);
			return climber;
		} 
		catch (JSONException e) 
		{
			throw new VertigoUnexpectedFormatException("_computeClimber - operation failed", e);
		}
	}

	@Override
	public ArrayList<Ascent> getAscents() throws VertigoException 
	{
		if ( this.ascents==null )
		{
			this.ascents = this._computeAscents();
		}
		return this.ascents;
	}
	
	private ArrayList<Ascent> _computeAscents() throws VertigoException 
	{
		ArrayList<Ascent> ascents = new ArrayList<Ascent>();
		try 
		{
			JSONObject ascentsObject = this.jsonObject.getJSONObject("ascents");
			JSONArray ascentsArray =  ascentsObject.getJSONArray("ascents");
			
			for (int i = 0; i < ascentsArray.length(); ++i) 
			{
				JSONObject ascentObject = (JSONObject) ascentsArray.get(i);
				
				Ascent ascent = new Ascent().initialize(ascentObject);
				ascents.add(ascent);
			}
			
		} 
		catch (JSONException e) 
		{
			throw new VertigoUnexpectedFormatException("computeAscentList - operation failed", e);
		}
		catch (VertigoException e) 
		{
			throw e ; 
		}
		catch (Exception e) 
		{
			throw new VertigoException("computeAscentList - operation failed", e);
		}
		return ascents;
	}

	private void parseRawData() throws VertigoException 
	{
		try 
		{
			JSONObject jo = (JSONObject) (new JSONTokener(this.rawData).nextValue());
			if (jo == null) 
			{
				throw new VertigoUnexpectedFormatException("parseRawData - operation failed - JSON object is null");
			}
			this.jsonObject = jo;
		} 
		catch (JSONException e) 
		{
			throw new VertigoUnexpectedFormatException("parseRawData - operation failed", e);
		}
	}

}
