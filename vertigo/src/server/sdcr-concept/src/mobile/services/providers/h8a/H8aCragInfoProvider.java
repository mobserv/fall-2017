package mobile.services.providers.h8a;

import com.google.appengine.labs.repackaged.org.json.*;
import mobile.services.facilities.*;
import mobile.services.model.Crag;

public class H8aCragInfoProvider
{
	private String rawData;
	private JSONObject jsonObject;
	private Crag crag;

	public H8aCragInfoProvider initialize(String data) throws VertigoException 
	{
		this.rawData = data;
		this.parseRawData();
		
		return this; //Just to allow chaining
	}

//	public String getRawData() 
//	{
//		return this.rawData;
//	}

	public Crag getCrag() throws VertigoException 
	{
		if ( this.crag==null )
		{
			this.crag = this._computeCrag();
		}
		return this.crag;
	}
	
	private Crag _computeCrag() throws VertigoException 
	{
		try 
		{
			if ( this.jsonObject!=null )
			{
				JSONObject jsonObject = this.jsonObject.getJSONObject("_source");
				Crag crag = new Crag().initialize(jsonObject);
				return crag;
			}
			else 
			{
				return null;
			}
		} 
		catch (JSONException e) 
		{
			throw new VertigoUnexpectedFormatException("_computeCrag - operation failed", e);
		}
	}

	private void parseRawData() throws VertigoException 
	{
		try 
		{
			JSONObject jo = (JSONObject) (new JSONTokener(this.rawData).nextValue());
			if (jo == null) 
			{
				throw new VertigoUnexpectedFormatException("parseRawData - operation failed - JSON object is null");
			}
			JSONArray hitsArray =  jo.getJSONArray("hits");
			if ( hitsArray==null )
			{
				throw new VertigoUnexpectedFormatException("parseRawData - operation failed - Hits array is null");
			}
			
			if ( hitsArray.length()>0)
			{
				jo = (JSONObject) hitsArray.get(0);  //##CR - only interested on the first result!
				if (jo == null) 
				{
					throw new VertigoUnexpectedFormatException("parseRawData - operation failed - hits[0] null");
				}
				
				this.jsonObject = jo;
			}
			else
			{
				this.jsonObject = null;
			}
		} 
		catch (JSONException e) 
		{
			throw new VertigoUnexpectedFormatException("parseRawData - operation failed", e);
		}
	}

}
