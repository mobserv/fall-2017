package mobile.services.providers.h8a;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.urlfetch.*;

import mobile.services.facilities.JsonOperationResult;
import mobile.services.facilities.OperationResult;
import mobile.services.facilities.VertigoException;
import mobile.services.graph.ScoreGraphInfo;
import mobile.services.model.*;

public class H8aFacade {

	private static final Logger logger = Logger.getLogger(H8aFacade.class.getName());
	private static volatile H8aFacade instance = null;  //volatile to be MT-Safe
	
	protected H8aFacade() {
	}

	public static H8aFacade getInstance() {
		
		if (instance == null) {
			synchronized (Grades.class) {
				if (instance == null) {  //Double-check to be MT-Safe!
					instance = new H8aFacade();
				}
			}
		}
		return instance;
	}
	
	public OperationResult fetchData(String ID)
	{
		try
		{
			String jsonObjectAsString = this._fetchDataFromExternalSource(ID);
			OperationResult operationResult = new JsonOperationResult(jsonObjectAsString);
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}
	
	private String _fetchDataFromExternalSource(String ID) throws VertigoException
	{
		if ( ID==null ) 
		{
			throw new VertigoException("fetchDataFromExternalSource - operation failed - ID is null");
		}
		else
		{
			logger.log(Level.INFO, "Fetching data for user:" + ID);
		}
		
		String webSite8aUrl= "https://beta.8a.nu/api/scorecard/routes/{1}/all";
		String scoreCardUrl = webSite8aUrl.replace("{1}", ID);
		
		return fetchDataFromUrl(scoreCardUrl, true);
	}

	public String fetchDataFromUrl(String urlAsString, boolean asJson) throws VertigoException
	{
		try 
		{
			String content ;
			URL url = new URL(urlAsString);
		    URLFetchService url_service = URLFetchServiceFactory.getURLFetchService();
		    
		    FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();
		    //fetchOptions.doNotValidateCertificate();
		    //fetchOptions.doNotFollowRedirects();
		    //fetchOptions.setDeadline(1D); //##CR - to simulate network issues - Error 500 - Timeout Exception   
		    fetchOptions.setDeadline(60D);
		    
		    HTTPRequest request = new HTTPRequest(url, HTTPMethod.GET, fetchOptions);
		    if ( asJson )
		    {		    	
		    	request.setHeader(new HTTPHeader("Content-Type", "application/json; charset=UTF-8"));
		    }
		    else
		    {
		    	request.addHeader(new HTTPHeader("Referer", "https://www.8a.nu"));
		    	//request.addHeader(new HTTPHeader("Referer", "https://www.8a.nu/User/Profile.aspx?UserId=2293"));
		    	//request.addHeader(new HTTPHeader("Referer", "https://www.8a.nu/?IncPage=https%3A%2F%2Fwww.8a.nu%2FUser%2FProfile.aspx%3FUserId%3D2293"));
		    		
		    	request.setHeader(new HTTPHeader("Content-Type", "text/html; charset=UTF-8"));
		    	
		    }
		    //request.setPayload(json.getBytes("utf8"));
		    
		    HTTPResponse response = url_service.fetch(request);
		    if (response.getResponseCode() != 200) 
		    {
		    	throw new VertigoException("fetchDataFromUrl - " + response.getContent(), response.getResponseCode());
		    }
		    else
		    {
		    	content = new String(response.getContent());
		    }	   
		    
		    return content;
		}
		catch ( Exception e )
		{
			throw new VertigoException("fetchDataFromUrl - operation failed", e);
		}
	}

	public OperationResult fetchAscents(String ID) 
	{
		try
		{
			String data = this._fetchDataFromExternalSource(ID);
			H8aContentProvider provider = new H8aContentProvider().initialize(data);
			ArrayList<Ascent> ascentList = provider.getAscents();
			OperationResult operationResult = new OperationResult(ascentList );
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}
	
	public OperationResult fetchRanking(String commaSeparatedIDs)
	{
		try
		{
			String[] IDs = commaSeparatedIDs.split(",");
			
			ScoreGraphInfo graphInfo = new ScoreGraphInfo();
			for ( String ID: IDs)
			{
				H8aContentProvider provider = this.getContentProvider(ID);
				graphInfo.append(new TopTenContentProvider(provider).organizeData());
			}
			
			graphInfo.normalize();
			
			OperationResult operationResult = new OperationResult(graphInfo);
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}
	
	private H8aContentProvider getContentProvider(String ID) throws VertigoException 
	{
		String data = this._fetchDataFromExternalSource(ID);
		H8aContentProvider provider = new H8aContentProvider();
		
		provider.initialize(data);
		return provider;
	}

	public Comparator<Ascent> getByScoreComparer() {
		Comparator<Ascent> byScore = new Comparator<Ascent>() {

			@Override
			public int compare(Ascent ascent1, Ascent ascent2) {
				
				//By Score (Descending order)
				int diff = ascent1.getScore() - ascent2.getScore();
				if ( diff!=0 )
				{
					return -diff;
				}
				else
				{
					return -(ascent1.getDateAsInt() - ascent2.getDateAsInt()); 
				}
			}
		};
		
		return byScore;
	}

	public OperationResult fetchExternalClimberData(String externalReference) {
		try
		{
			String data = this._fetchDataFromExternalSource(externalReference);
			H8aContentProvider provider = new H8aContentProvider().initialize(data);
			Climber climber = provider.getClimber();
			OperationResult operationResult = new OperationResult(climber);
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}

	public OperationResult fetchHtmlData(String ID) 
	{
		try
		{
			String htmlContent = this._fetchHtmlDataFromExternalSource(ID);
			OperationResult operationResult = new OperationResult(htmlContent);
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}
	
	private String _fetchHtmlDataFromExternalSource(String ID) throws VertigoException
	{
		if ( ID==null ) 
		{
			throw new VertigoException("fetchDataFromExternalSource - operation failed - ID is null");
		}
		else
		{
			logger.log(Level.INFO, "Fetching HTML data for user:" + ID);
		}
		
		return fetchHtmlDataFrom8a(ID);
	}

	public String fetchHtmlDataFrom8a(String userID) throws VertigoException
	{
		//String urlAsString= "https://www.8a.nu/User/Profile.aspx?UserId={1}";
		String urlAsString="https://www.8a.nu/?IncPage=https%3A%2F%2Fwww.8a.nu%2FUser%2FProfile.aspx%3FUserId%3D{1}";
		urlAsString = urlAsString.replace("{1}", userID);
		
		try 
		{
			String content ;
			
			URL url = new URL(urlAsString);
		    URLFetchService url_service = URLFetchServiceFactory.getURLFetchService();
		    
		    FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();
		    //fetchOptions.doNotValidateCertificate();
		    fetchOptions.doNotFollowRedirects();
		    //fetchOptions.setDeadline(1D); //##CR - to simulate network issues - Error 500 - Timeout Exception   
		    fetchOptions.setDeadline(60D);
		    
		    HTTPRequest request = new HTTPRequest(url, HTTPMethod.GET, fetchOptions);
		    request.addHeader(new HTTPHeader("Referer", "https://www.8a.nu"));
	    	//request.addHeader(new HTTPHeader("Referer", "https://www.8a.nu/User/Profile.aspx?UserId=2293"));
	    	//request.addHeader(new HTTPHeader("Referer", "https://www.8a.nu/?IncPage=https%3A%2F%2Fwww.8a.nu%2FUser%2FProfile.aspx%3FUserId%3D2293"));
	    	request.setHeader(new HTTPHeader("Content-Type", "text/html; charset=UTF-8"));
		    //request.setPayload(json.getBytes("utf8"));
		    
		    HTTPResponse response = url_service.fetch(request);
		    if (response.getResponseCode() != 200) 
		    {
		    	throw new VertigoException("fetchDataFromUrl - " + response.getContent(), response.getResponseCode());
		    }
		    else
		    {
		    	for (HTTPHeader header : response.getHeaders() ) {
		    		
		    		logger.log(Level.WARNING, "...... Header: " + header.getName() + " > " + header.getValue() );
		    		if (header.getName().equalsIgnoreCase("set-cookie"))
		    		{
		    			HTTPHeader cookie = header;
		    			logger.log(Level.WARNING, "...... Cookie: " + cookie.getValue()); 
		    		}
		    	}
		    	
		    	content = new String(response.getContent());
		    }	   
		    
		    return content;
		}
		catch ( Exception e )
		{
			throw new VertigoException("fetchDataFromUrl - operation failed", e);
		}
	}

	public OperationResult fetchCrag(String countryCode, String cragName) 
	{
		try
		{
			String data = this._fetchCragDataFromExternalSource(countryCode, cragName);
			H8aCragInfoProvider provider = new H8aCragInfoProvider().initialize(data);
			Crag crag = provider.getCrag();
			OperationResult operationResult = new OperationResult( crag );
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}
	
	public String _fetchCragDataFromExternalSource(String countryCode, String cragName) throws VertigoException 
	{
		if ( cragName==null ) 
		{
			throw new VertigoException("fetchCragDataFromExternalSource - operation failed - cragName is null");
		}
		else
		{
			logger.log(Level.INFO, "Fetching data for crag:" + cragName);
		}
		
		//No direct access - a search query is required to get the crag data
		//Example: https://beta.8a.nu/api/search/filter?type=crag&country=FRA&ascentType=routes&query=peillon

		String webSite8aUrl= "https://beta.8a.nu/api/search/filter?type=crag&country={1}&ascentType=routes&query={2}";
		String searchCragUrl;
		try 
		{
			searchCragUrl = webSite8aUrl.replace("{1}", URLEncoder.encode(countryCode, "UTF-8"));
			searchCragUrl = searchCragUrl.replace("{2}", URLEncoder.encode(cragName, "UTF-8"));
		} 
		catch (UnsupportedEncodingException e) 
		{
			throw new VertigoException("Operation failed - check the root cause for more details.", e);
		}
		
		return fetchDataFromUrl(searchCragUrl, true);
	}

}
