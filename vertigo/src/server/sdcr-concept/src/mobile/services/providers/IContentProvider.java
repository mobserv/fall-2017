package mobile.services.providers;

import java.util.ArrayList;

import mobile.services.facilities.VertigoException;
import mobile.services.model.Ascent;
import mobile.services.model.Climber;

public interface IContentProvider {
	
	public Climber getClimber() throws VertigoException;
	public ArrayList<Ascent> getAscents() throws VertigoException;

}
