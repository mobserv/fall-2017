package mobile.services.providers.vertigo;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Ref;

import mobile.services.entities.EAscent;
import mobile.services.entities.EClimber;
import mobile.services.facilities.VertigoException;
import mobile.services.model.Ascent;
import mobile.services.model.Climber;
import mobile.services.providers.IContentProvider;

public class VertigoContentProvider implements IContentProvider
{
	private Climber climber;
	private ArrayList<Ascent> ascentList;

	public void initialize(String xID) throws VertigoException 
	{
		this.ascentList = new ArrayList<Ascent>();
		
		EClimber climberEntity = ofy().load().type(EClimber.class).filter("xID", xID).first().now();
		if ( climberEntity != null )
		{
			this.climber = new Climber().initialize(climberEntity);
			
//			Iterable<Key<AscentEntity>> allKeys = ofy().load().type(AscentEntity.class).filter("climberRef", Ref.create(climberEntity)).order("-ascentDate").keys();
//			for ( Key<AscentEntity> key: allKeys )
//			{
//				AscentEntity entity = ofy().load().type(AscentEntity.class).id(key.getId()).now();
//				ascentList.add(new Ascent().initialize(entity));
//			}
	
//
			//##CR - Order removed to reduce the number of reads because of the Quota restriction 
//			List<EAscent> entities = ofy().load().type(EAscent.class).filter("climberRef", Ref.create(climberEntity)).order("ascentDate").list();
			List<EAscent> entities = ofy().load().type(EAscent.class).filter("climberRef", Ref.create(climberEntity)).list();
			for ( EAscent entity: entities)
			{
				Ascent ascent = new Ascent().initialize(entity);
				if ( ascent.isDoneBefore1998() )
				{
					//-----------------------------------------------------------------------------
					//##CR - Ascent excluded by default - to reduce the size of the history shown!
					//-----------------------------------------------------------------------------
					//The number of years to be considered should be defined via user preferences!
					//-----------------------------------------------------------------------------
				}
				else
				{
					this.ascentList.add(ascent);
				}
			}
		}
	}


	@Override
	public Climber getClimber() throws VertigoException 
	{
		return this.climber;
	}

	@Override
	public ArrayList<Ascent> getAscents() throws VertigoException 
	{
		return this.ascentList;
	}
}
