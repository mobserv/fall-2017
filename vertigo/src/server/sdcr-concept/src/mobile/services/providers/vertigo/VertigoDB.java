package mobile.services.providers.vertigo;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Ref;

import mobile.services.entities.*;
import mobile.services.facilities.OperationResult;
import mobile.services.facilities.VertigoException;
import mobile.services.graph.ScoreGraphInfo;
import mobile.services.model.*;
import mobile.services.providers.IContentProvider;
import mobile.services.providers.h8a.H8aFacade;

public class VertigoDB {

	private static final Logger logger = Logger.getLogger(VertigoDB.class.getName());
	private static volatile VertigoDB instance = null;  //volatile to be MT-Safe
	
	static 
	{
		ObjectifyService.register(EClimber.class);
        ObjectifyService.register(ECountry.class);
        ObjectifyService.register(ECrag.class);
        ObjectifyService.register(EAscent.class);
        ObjectifyService.register(ENotification.class);
    }
	
	protected VertigoDB() {
	}

	public static VertigoDB getInstance() {
		
		if (instance == null) {
			synchronized (Grades.class) {
				if (instance == null) {  //Double-check to be MT-Safe!
					instance = new VertigoDB();
				}
			}
		}
		return instance;
	}
	
	public OperationResult fetchClimberData(String key)
	{
		try
		{
			Climber climber = this._fetchClimberData(key);
			OperationResult operationResult = new OperationResult(climber);
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}

	private Climber _fetchClimberData(String key) throws VertigoException
	{
		if ( key==null ) 
		{
			throw new VertigoException("fetchClimberData - operation failed - ID is null");
		}
		else
		{
			logger.log(Level.INFO, "Fetching data for climber Key:" + key);
			
			try 
			{
				long longId = Long.parseLong(key);
				EClimber climberEntity = ofy().load().type(EClimber.class).id(longId).now();
				if ( climberEntity==null )
				{
					throw new VertigoException("fetchClimberData - operation failed - Key does not exist - " + key);
				}
				
				return new Climber().initialize(climberEntity);
			} 
			catch (VertigoException e) 
			{
				throw e ; 
			}
			catch (Exception e) 
			{
				throw new VertigoException("fetchClimberData - operation failed", e);
			}
		}
	}

	public OperationResult persistClimber(Climber climber) 
	{
		try 
		{
			EClimber climberEntity = new EClimber(climber);
			ofy().save().entity(climberEntity).now();
			
			Ref<EClimber> reference = Ref.create(climberEntity);
			logger.log(Level.INFO, "Climber has been persisted - ID is: " + reference);
			return new OperationResult( new Climber().initialize(reference.get()) );
		} 
		catch ( Exception e)
		{
			logger.log(Level.SEVERE, "persistClimber - operationFailed", e);
			return new OperationResult(e);
		}
	}

	public OperationResult performUserRegistration(HttpServletRequest request) 
	{
		try
		{
			String email = request.getParameter("email");
			String xID = request.getParameter("xID");  //8a ID
			
			EClimber climberEntity = ofy().load().type(EClimber.class).filter("email", email).first().now();
			if ( climberEntity!=null )
			{
				throw new VertigoException("User already exists - " + email );
			}
			
			climberEntity = ofy().load().type(EClimber.class).filter("xID", xID).first().now();
			if ( climberEntity!=null )
			{
				throw new VertigoException("A user already references 8a.nu score card - " + xID );
			}
			
			Climber climber = null ;
			
			if (xID != null)
			{
				OperationResult operationResult = H8aFacade.getInstance().fetchExternalClimberData(xID);
				if ( operationResult.getApplicationStatus() == 0 )
				{
					climber = (Climber) (operationResult.getApplicationData());
					climber.setData(request); 
				}
			}
			
			if ( climber==null) 
			{
				climber = new Climber().setData(request);
			}
			
			OperationResult operationResult = this.persistClimber(climber);
			return operationResult;
		} 
		catch ( Exception e)
		{
			logger.log(Level.SEVERE, "persistClimber - operationFailed", e);
			return new OperationResult(e);
		}
		
	}

	public OperationResult fetchUserData(HttpServletRequest request) 
	{
		String email = request.getParameter("email");
		if ( email!=null)
		{
			return fetchUserDataByMail(email);
		}
		else 
		{
			String xID = request.getParameter("xID");
			if ( xID==null )
			{
				xID = request.getParameter("ID");  //##CR for some stupid backward compatibility only
			}
			return fetchUserDataByExternalID(xID);
		}	
	}
	
	public OperationResult fetchUserDataByMail(String email) 
	{
		try
		{
			EClimber climberEntity = ofy().load().type(EClimber.class).filter("email", email).first().now();
			if ( climberEntity==null )
			{
				throw new VertigoException("User does not exists: " + email );
			}
			else
			{
				Climber climber = new Climber().initialize(climberEntity);
				return new OperationResult( climber );
			}
		}		
		catch ( VertigoException e)
		{
			logger.log(Level.SEVERE, "fetchUserData - operation failed", e);
			return new OperationResult(e);
		}
		catch ( Exception e)
		{
			logger.log(Level.SEVERE, "fetchUserData - operation failed", e);
			return new OperationResult(e);
		}
	}
	
	public OperationResult fetchUserDataByExternalID(String xID) 
	{
		try
		{
			EClimber climberEntity = ofy().load().type(EClimber.class).filter("xID", xID).first().now();
			if ( climberEntity==null )
			{
				throw new VertigoException("User does not exists: " + xID );
			}
			else
			{
				Climber climber = new Climber().initialize(climberEntity);
				return new OperationResult( climber );
			}
		}		
		catch ( VertigoException e)
		{
			logger.log(Level.SEVERE, "fetchUserData - operation failed", e);
			return new OperationResult(e);
		}
		catch ( Exception e)
		{
			logger.log(Level.SEVERE, "fetchUserData - operation failed", e);
			return new OperationResult(e);
		}
	}

	public OperationResult performUserLogon(HttpServletRequest request) 
	{
		try
		{
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String importData = request.getParameter("importData");
			
			EClimber climberEntity = ofy().load().type(EClimber.class).filter("email", email).first().now();
			if ( climberEntity==null )
			{
				throw new VertigoException("Authentication failed - Check the email address: " + email );
			}
			else
			{
				Climber climber = new Climber().initialize(climberEntity);
				if ( password.equals(climber.getPassword()) )
				{
					OperationResult operationResult = new OperationResult( climber );
					if ( importData!=null )
					{
						String xId = climber.getExternalReference();
						operationResult = H8aFacade.getInstance().fetchAscents(xId);
						operationResult = this.doImport(xId, operationResult);
					}
					
					return operationResult ;
				}
				else 
				{
					throw new VertigoException("Authentication failed - 3 attempts remaining for user:" + email );
				}
			}
		}		
		catch ( VertigoException e)
		{
			logger.log(Level.SEVERE, "performUserLogon - operation failed", e);
			return new OperationResult(e);
		}
		catch ( Exception e)
		{
			logger.log(Level.SEVERE, "performUserLogon - operation failed", e);
			return new OperationResult(e);
		}
	}

	public OperationResult fetchAscents(String xID) {
		
		try
		{
			IContentProvider provider = this.getContentProvider(xID);
			ArrayList<Ascent> ascentList = provider.getAscents();
			
			OperationResult operationResult = new OperationResult(ascentList );
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}

	public OperationResult fetchRanking(String commaSeparatedIDs) 
	{
		try
		{
			String[] IDs = commaSeparatedIDs.split(",");
			
			ScoreGraphInfo graphInfo = new ScoreGraphInfo();
			for ( String ID: IDs)
			{
				String mode = this.getPreferredMode(ID);
				if ( "V".equals(mode) )
				{
					IContentProvider provider = this.getContentProvider(ID);
					graphInfo.append(new TopTenContentProvider(provider).organizeData());
				}
				else
				{
					//-----------------------------------------------------------------------
					// ##CR - The difficulty here is that we may have to compare a ranking 
					// coming from VertigoDB with some others coming form an external source
					//-----------------------------------------------------------------------
					OperationResult or = H8aFacade.getInstance().fetchRanking(ID);
					ScoreGraphInfo subScoreGraphInfo = (ScoreGraphInfo) or.getApplicationData();
					graphInfo.append(subScoreGraphInfo);
				}
			}
			
			graphInfo.normalize();
			
			OperationResult operationResult = new OperationResult(graphInfo);
			return operationResult;
		}
		catch (VertigoException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}

	private IContentProvider getContentProvider(String xID) throws VertigoException 
	{
		VertigoContentProvider provider = new VertigoContentProvider();
		
		provider.initialize(xID);
		return provider;
	}
	
	public OperationResult doImport(String xID, OperationResult operationResult) 
	{
		OperationResult newResult = operationResult;
		
		try
		{
			this._doImport(xID, operationResult);
		}
		catch ( VertigoException e ) 
		{
			newResult = new OperationResult(e);
		}
		
		return newResult ;
	}
	
	
	private void _doImport(String xID, OperationResult operationResult) throws VertigoException 
	{
		//this._doResetCountries(); //##CR - todo - remove it!
		//this._doResetCrags(); //##CR - todo - remove it!
		
		EClimber climberEntity = ofy().load().type(EClimber.class).filter("xID", xID).first().now();
		if ( climberEntity == null )
		{  		
//			climberEntity = new EClimber(xID);
//			ofy().save().entity(climberEntity).now();
			throw new VertigoException("No climber references this external ID: " + xID + " - Make sure to first register the climber with a ref to an 8a.nu scorecard!");
		}
		else 
		{
			this._doResetAscents(climberEntity);
			
			@SuppressWarnings("unchecked")
			ArrayList<Ascent> ascentList = (ArrayList<Ascent>)(operationResult.getApplicationData());
			if ( ascentList!=null )
			{
				for ( final Ascent ascent : ascentList )
				{
					ECountry country = this._doImportCountry(climberEntity, ascent);
					if ( !country.getCode().equals("?") )
					{
						ECrag crag = this._doImportCrag(climberEntity, ascent, country);
					}
					
					EAscent ascentEntity = new EAscent(Ref.create(climberEntity), ascent);
					ofy().save().entity(ascentEntity);
				}
			}
			
			//Update the climber data to enable the Vertigo node
			climberEntity.forceVertigoMode();
			ofy().save().entity(climberEntity).now();
		}
	}

	private void _doResetAscents(EClimber climberEntity)
	{
		Iterable<Key<EAscent>> allKeys = ofy().load().type(EAscent.class).filter("climberRef", Ref.create(climberEntity)).keys();
		ofy().delete().keys(allKeys);
	}
	
	private ECountry _doImportCountry(EClimber climberEntity, Ascent ascent)
	{
		String code = ascent.getCountryCode();
		if ( code==null || code.isEmpty() ) 
		{
			code="?"; //Entity @ID cannot be null or Empty!
		}
		
		ECountry country = ofy().load().type(ECountry.class).id(code).now();
		if ( country==null )
		{
        	country = new ECountry(code);
			ofy().save().entity(country).now();
		}
		
		return country;
	}
	
	private void _doResetCountries()
	{
		Iterable<Key<ECountry>> allKeys = ofy().load().type(ECountry.class).keys();
		ofy().delete().keys(allKeys);
	}
	
	private ECrag _doImportCrag(EClimber climberEntity, Ascent ascent, ECountry country) throws VertigoException 
	{
		if ( ascent.getCragName()!=null )
		{
			String code = country.getCode() + "|" + ascent.getCragName().toUpperCase(Locale.FRENCH);
			
			ECrag crag = ofy().load().type(ECrag.class).id(code).now();
			if ( crag==null )
			{
	        	crag = new ECrag(Ref.create(country), ascent.getCragName());
	        	ofy().save().entity(crag).now();
			}
		
			return crag;
		}
		else
		{
			throw new VertigoException("Unexpected situation - cragName is null!");
		}
	}

	public OperationResult enrichCragData(HttpServletRequest request) 
	{
		OperationResult operationResult;
		
		try
		{
			this._doEnrichCrags();
			operationResult = new OperationResult("operation succeeded");
		}
		catch ( Exception e ) 
		{
			operationResult = new OperationResult(e);
		}
		
		return operationResult ;
	}
	
	private void _doEnrichCrags() throws VertigoException 
	{
		List<ECrag> entities = ofy().load().type(ECrag.class).list();
		for ( ECrag entity: entities)
		{
			this._doEnrichCragData(entity);
		}
	}
	
	private void _doEnrichCragData(ECrag cragEntity) 
	{
		//Get additional data from 8a.nu !
		OperationResult operationResult = H8aFacade.getInstance().fetchCrag(cragEntity.getCountryCode(), cragEntity.getName());
		if ( operationResult.getApplicationData()!=null )
		{
			Crag crag = (Crag)operationResult.getApplicationData();
			
			//We got the additional (i.e.: crag location) data let's update the crag information
			cragEntity.setCity(crag.getCity());
			cragEntity.setExternalReference(crag.getExternalReference());
			cragEntity.setLongitude(crag.getLongitude());
			cragEntity.setLatitude(crag.getLatitude());
			cragEntity.setComment(crag.getComment());
			
			ofy().save().entity(cragEntity).now();
			logger.info("Crag has been update: " + cragEntity.getCode());
		}
		else
		{
			//logger.warning("External source does not contain additional information for: "+ cragEntity.getCountryCode()+"|"+cragEntity.getName());
		}
	}

	private void _doResetCrags()
	{
		Iterable<Key<ECrag>> allKeys = ofy().load().type(ECrag.class).keys();
		ofy().delete().keys(allKeys);
	}

	public OperationResult storeNewAscent(HttpServletRequest request) 
	{
		try
		{
			String xID = request.getParameter("xID");  //8a ID
			EClimber climberEntity = ofy().load().type(EClimber.class).filter("xID", xID).first().now();
			if ( climberEntity==null )
			{
				throw new VertigoException("Climber not found for external reference:" + xID );
			}
			Ascent ascent = new Ascent().initialize(request);
			
			EAscent ascentEntity = new EAscent(Ref.create(climberEntity), ascent);
			ofy().save().entity(ascentEntity).now();
			
			Ref<EAscent> reference = Ref.create(ascentEntity);
			
			this.generateNotifications(reference, climberEntity);

			logger.log(Level.INFO, "New ascent has been persisted - ID is: " + reference);
			return new OperationResult( new Ascent().initialize(reference.get()), "New ascent has been persisted" );
		} 
		catch ( Exception e)
		{
			logger.log(Level.SEVERE, "persistClimber - operationFailed", e);
			return new OperationResult(e);
		}
	}

	private void generateNotifications(Ref<EAscent> ascentRef, EClimber senderEntity) {
		
		//Iterate through the climber list ... 
		// and generate a new notification for all of them except for the sender
		
		List<EClimber> entities = ofy().load().type(EClimber.class).list();
		for ( EClimber entity: entities)
		{
			if ( entity.getUID() != senderEntity.getUID() )
			{
				ENotification nofificationEntity = new ENotification(ascentRef, Ref.create(entity));
				ofy().save().entity(nofificationEntity).now();
			}
		}
	}

	public String getPreferredMode(String xID) {
		
		//Let's determine the mode according to the climber settings
		Climber climber = (Climber) VertigoDB.getInstance().fetchUserDataByExternalID(xID).getApplicationData();
		if ( climber!=null )
		{
			return climber.getPreferredMode();  //Returns V when the data has been imported to VertigoDB
		}
		else
		{
			return null;
		}
	}

	public OperationResult clearNotifications(String xID) {
		
		return this.handleRequest4Notifications(xID, true);
	}
	public OperationResult fetchNotifications(String xID) {
		
		return this.handleRequest4Notifications(xID, false);
	}
	
	private OperationResult handleRequest4Notifications(String xID, boolean clearNotification) {
		ArrayList<Ascent> ascents = new ArrayList<Ascent>();
		
		EClimber climberEntity = ofy().load().type(EClimber.class).filter("xID", xID).first().now();
		if ( climberEntity != null )
		{
			//Fetches the notifications targeting a particular user
			List<ENotification> entities = ofy().load().type(ENotification.class).filter("sentToRef", Ref.create(climberEntity)).list();
			if ( clearNotification )
			{
				ofy().delete().entities(entities); //Asynchronous delete of the collection
			}
			else
			{				
				for ( ENotification entity: entities)
				{
					try 
					{
						if ( entity.getAscentEntity() !=null )
						{
							Ascent ascent = new Ascent().initialize(entity.getAscentEntity());
							ascent.enrichWith(entity.getAscentEntity().getClimberEntity());
							ascents.add(ascent);
						}
					} 
					catch (VertigoException e) 
					{
						logger.log(Level.SEVERE, e.getMessage(), e);
					}
					
				}
			}
		}

		OperationResult operationResult = new OperationResult(ascents);
		return operationResult;

	}

	public ArrayList<Crag> getCrags() {
		
		// TODO Auto-generated method stub
		return null;
	}

	public OperationResult fetchCrags(String xID) 
	{
		//@todo - add filtering capabilities (by country, by climber)
		try
		{
			ArrayList<Crag> crags = new ArrayList<>();
			List<ECrag> entities = ofy().load().type(ECrag.class).list();
			for ( ECrag entity: entities)
			{
				Crag crag = new Crag().initialize(entity);
				if ( crag.getLatitude()!=0 && crag.getLongitude()!=0 )
				{
					crags.add(crag);
				}
			}
			
			OperationResult operationResult = new OperationResult(crags);
			return operationResult;
		}
//		catch (VertigoException e)
//		{
//			logger.log(Level.SEVERE, e.getMessage(), e);
//			return new OperationResult(e);
//		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}

	public OperationResult fetchClimbers() {
		
		//@todo - add filtering capabilities (by team) - all climbers are retrieved for now
		try
		{
			ArrayList<Climber> climbers = new ArrayList<>();
			List<EClimber> entities = ofy().load().type(EClimber.class).list();
			for ( EClimber entity: entities)
			{
				Climber climber = new Climber().initialize(entity);
				climbers.add(climber);
			}
			
			OperationResult operationResult = new OperationResult(climbers);
			return operationResult;
		}
//		catch (VertigoException e)
//		{
//			logger.log(Level.SEVERE, e.getMessage(), e);
//			return new OperationResult(e);
//		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}

	public OperationResult fetchCountries() 
	{
		try
		{
			ArrayList<Country> countries = new ArrayList<>();
			List<ECountry> entities = ofy().load().type(ECountry.class).list();
			for ( ECountry entity: entities)
			{
				Country country = new Country().initialize(entity);
				countries.add(country);
			}
			
			OperationResult operationResult = new OperationResult(countries);
			return operationResult;
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return new OperationResult(e);
		}
	}
}
