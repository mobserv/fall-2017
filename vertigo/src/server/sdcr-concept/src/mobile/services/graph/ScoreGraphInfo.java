package mobile.services.graph;

import java.util.ArrayList;

import mobile.services.facilities.Helper;
import mobile.services.facilities.VertigoException;
import mobile.services.model.TopTenContentProvider;

public class ScoreGraphInfo {
	
	ArrayList<Year> years = new ArrayList<Year>();
	ArrayList<LineChartInfo> lineChartInfos = new ArrayList<LineChartInfo>();
	
	public String toString()
	{
		return "ScoreGraphInfo - lineChartInfos.size(): " + this.lineChartInfos.size();
	}

	public void append(ScoreGraphInfo subScoreGraphInfo) 
	{
		if ( subScoreGraphInfo != null )
		{
			for ( LineChartInfo lineChartInfo: subScoreGraphInfo.lineChartInfos )
			{
				lineChartInfos.add(lineChartInfo);
			}
		}
	}
	
	public void append(TopTenContentProvider contentProvider) throws VertigoException 
	{
		lineChartInfos.add(new LineChartInfo(contentProvider));
	}

	public void normalize() 
	{
		int firstYear = this.getFirstAscentYear();
		int lastYear = Helper.getCurrentYear(); 
		
		for (int index=firstYear; index<=lastYear; index++)
		{
			years.add(new Year("" + index));
		}
		
		years.add(new Year("now"));
		years.add(new Year("all"));

		for ( LineChartInfo lineChartInfo : lineChartInfos )
		{
			lineChartInfo.organise(years);
		}
	}

	private int getFirstAscentYear() 
	{
		int firstAscentYear = 0;
		for ( LineChartInfo lineChartInfo : lineChartInfos )
		{
			int fay = lineChartInfo.getFisrtAscentYear();
			if ( fay < firstAscentYear || firstAscentYear==0 )
			{
				firstAscentYear = fay;
			}
		}
		return firstAscentYear;
	}

}
