package mobile.services.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mobile.services.facilities.VertigoException;
import mobile.services.model.Ascent;
import mobile.services.model.Climber;
import mobile.services.model.TopTenContentProvider;

public class LineChartInfo {

	private transient TopTenContentProvider contentProvider;
	
	private Climber climber;
	private ArrayList<Integer> scores = new ArrayList<Integer>();
	private HashMap<String, List<Ascent>> topTenAscentsPerYear = new HashMap<String, List<Ascent>>();

	public LineChartInfo(TopTenContentProvider contentProvider) throws VertigoException 
	{
		this.contentProvider = contentProvider ;
		this.climber = contentProvider.getClimber();
	}

	public String toString()
	{
		return "LineChartInfo - climber: " + this.climber + " - topTenAscentsPerYear.size(): " + this.topTenAscentsPerYear.size();
	}

	public void organise(ArrayList<Year> years) 
	{
		this.scores = new ArrayList<Integer>();
		
		for ( Year yearObject: years )
		{
			int score = contentProvider.computeScoreForYear(yearObject.year);
			this.scores.add(score);
		}
		
		this.topTenAscentsPerYear = contentProvider.GetTopTenCollections();
	}

	public int getFisrtAscentYear() 
	{
		return contentProvider.getFirstAscentYear();
	}
}
