package mobile.services.graph;

public class Year {

	String year ;
	String label;
	
	public Year(String year) { 
		this.year = year;
		
		if (year=="all") 
		{
			label = "All Time";
		}
		else if (year=="now") 
		{
			label = "Past 12 months"; 
		}
		else
		{
			this.label = year;
		}
	}
}
