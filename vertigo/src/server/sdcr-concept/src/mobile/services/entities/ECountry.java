package mobile.services.entities;

import com.googlecode.objectify.annotation.*;

@Entity
public class ECountry 
{
	public ECountry()
	{
		//no-arg constructor is mandatory when using objectify
	}
	
	public ECountry(String countryCode) 
	{
		this.code = countryCode; 
	}

	@Id String code;
	
	public String getCode() 
	{
		return this.code;
	}
}