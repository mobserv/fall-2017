package mobile.services.entities;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class ENotification 
{

	public ENotification ()
	{
		//no-arg constructor is mandatory when using objectify
	}
	
	public ENotification(Ref<EAscent> ascent, Ref<EClimber> sentTo ) {
		
		this.ascentRef = ascent;
		this.sentToRef = sentTo;
	}

	@Id Long _id; //ID - Auto-generated value by GAE
	@Index Ref<EClimber> sentToRef;
	Ref<EAscent> ascentRef;
	
	public EAscent getAscentEntity() {
		return ascentRef.get();
	}
}
