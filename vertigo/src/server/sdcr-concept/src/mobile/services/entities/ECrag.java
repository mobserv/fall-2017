package mobile.services.entities;

import com.googlecode.objectify.*;
import com.googlecode.objectify.annotation.*;

@Entity
public class ECrag {

    public static class Optimized {}

    //@Id Long _id;  //ID - Auto-generated value by GAE
    
    @Id String code;
    @Index String name;
    
    @Load(unless=Optimized.class)
    @Index Ref<ECountry> country;
	private String city;
	private String externalReference;
	private double longitude;
	private double latitude;
	private String comment;
    
	public ECrag()
	{
		//no-arg constructor is mandatory for objectify 
	}
	
	public ECrag(Ref<ECountry> country, String name) 
	{
		this.code = country.getValue().getCode() + "|" + name.toUpperCase();  //Hack to get rid of 8a.nu duplicates!!! 
		this.name = name;
		this.country = country;
	}

	@Override
	public String toString()
	{
		return "Crag: (" + code + ") - name: " + name + " - " + country.get().getCode();
	}

	public String getCountryCode() 
	{
		return country.get().getCode();
	}

	public String getName() 
	{
		return this.name;
	}

	public void setCity(String city) 
	{
		this.city = city;
	}
	
	public void setExternalReference(String xID) 
	{
		this.externalReference = xID;
	}

	public String getCode() 
	{
		return this.code;
	}

	public void setLongitude(double longitude) 
	{
		this.longitude = longitude;
	}
	public void setLatitude(double latitude) 
	{
		this.latitude = latitude;
	}

	public void setComment(String comment) 
	{
		this.comment = comment;
	}

	public String getComment() {
		
		return this.comment;
	}
	
	public String getCity() {
		
		return this.city;
	}
	
	public String getExternalReference() {
		
		return this.externalReference;
	}

	public double getLongitude() 
	{
		return this.longitude;
	}
	
	public double getLatitude() 
	{
		return this.latitude;
	}
}


