package mobile.services.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import mobile.services.model.Climber;

@Entity
public class EClimber {

	public EClimber()
	{
		//no-arg constructor is mandatory when using objectify
	}
	
//	public EClimber(String xID) 
//	{
//		this.firstName = "todo " + xID;
//		this.xID = xID;
//	}

	public EClimber(Climber climber) 
	{
		this.firstName = climber.getFirstName();
		this.lastName = climber.getLastName();
		this.xID = climber.getExternalReference();
		this.email = climber.getEMail();
		this.password = climber.getPassword();
		this.cityName= climber.getCityName();
		this.countryCode = climber.getCountryCode();
		this.sex = climber.getSex();
		this.height = climber.getHeight();
		this.weight = climber.getWeight();
		this.avatar = climber.getAvatar();
		this.preferredMode = ""; 
	}
	
	@Id Long _id;	//ID - Auto-generated value by GAE
	
	@Index String xID;  //ID to external source (such as 8a.nu)
	@Index String firstName;
	@Index String lastName;
	
	@Index String email;	 //Used to log in - Should be moved to User entity
	@Index String password;  //Used to log in - Should be moved to User entity
	
	String cityName;
	String countryCode;
	Integer sex;
	Integer height;
	Integer weight;
	String avatar;
	String preferredMode;  //Set to V when data is imported
	
	public Long getUID() {
		return _id;
	}
	
	public String getPassword() 
	{
		return this.password;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getExternalReference() {
		return this.xID;
	}

	public String getEMail() {
		return this.email;
	}

	public String getCityName() {
		return this.cityName;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public Integer getSex() {
		return this.sex;
	}

	public Integer getHeight() {
		return this.height;
	}

	public Integer getWeight() {
		return this.weight;
	}

	public String getAvatar() {
		return this.avatar;
	}

	public String getPreferredMode() {
		//return "V"; //##CR - Used to force VertigoDB as the source when 8a.nu is down!
		return this.preferredMode;
	}

	public void forceVertigoMode() {
		this.preferredMode = "V";
	}

	public String getFullName() {
		return this.getFirstName() + " " + getLastName();
	}

}
