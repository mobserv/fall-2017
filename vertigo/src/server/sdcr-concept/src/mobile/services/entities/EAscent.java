package mobile.services.entities;

import java.util.Date;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import mobile.services.model.Ascent;

@Entity
public class EAscent {

	public EAscent()
	{
		//no-arg constructor is mandatory when using objectify
	}
	
	public EAscent(Ref<EClimber> climber, Ascent ascent) {
		
		this.climberRef = climber;
		this.routeName = ascent.getRouteName();
		this.cragName = ascent.getCragName();
		this.sectorName = ascent.getSectorName();
		
		this.grade = ascent.getGradeIndex(); 
		this.score = ascent.getScore();
		this.style = ascent.getStyle();
		
		this.countryCode = ascent.getCountryCode();
		this.ascentDate = ascent.getAscentDate();
	}

	@Id Long _id; //ID - Auto-generated value by GAE
	
	@Index Ref<EClimber> climberRef;
	
	@Index String routeName;
	@Index String cragName;
	@Index String sectorName;
	
	@Index Integer grade;
	@Index Integer style;
	@Index Integer score;
	
	String comment;
	String countryCode;
	
	@Index Date ascentDate;

	public String getRouteName() 
	{
		return routeName;
	}

	public String getCragName() 
	{
		return this.cragName;
	}

	public String getSectorName() {
		return this.sectorName;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public int getGradeIndex() {
		return this.grade;
	}

	public int getMethodIndex() {
		return this.style;
	}

	public int getScore() {
		return this.score;
	}

	public Date getAscentDate() {
		return this.ascentDate;
	}

	public EClimber getClimberEntity() {
		return this.climberRef.get() ;
	}
}
