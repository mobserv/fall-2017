package mobile.services.facilities;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class JsonHelper 
{
	private static final Logger logger = Logger.getLogger(JsonHelper.class.getName());
	
	public static JSONObject getInfoAsObject(JSONObject jsonObject, String key) {

		JSONObject result = null;

		try {
			result = jsonObject.getJSONObject(key);
		} catch (JSONException e) {
			logger.log(Level.WARNING, "getInfoAsObject - missing json attribute: " + key);
		}

		return result;
	}
	
	public static String getInfoAsString(JSONObject jsonObject, String key) {

		String result = null;

		try {
			result = jsonObject.getString(key);
		} catch (JSONException e) {
			logger.log(Level.WARNING, "getInfoAsString - missing json attribute: " + key);
		}

		return result;
	}

	public static int getInfoAsInt(JSONObject jsonObject, String key) {

		int result = 0;

		if (jsonObject!=null)
		{
			try {
				result = jsonObject.getInt(key);
			} catch (JSONException e) {
				logger.log(Level.WARNING, "getInfoAsInt - missing json attribute: " + key);
			}
		}
		return result;
	}

	public static long getInfoAsLong(JSONObject jsonObject, String key) {
		long result = 0L;

		if (jsonObject!=null)
		{
			try {
				result = jsonObject.getLong(key);
			} catch (JSONException e) {
				logger.log(Level.WARNING, "getInfoAsLong  - missing json attribute: " + key);
			}
		}
		return result;
	}
	
	public static double getInfoAsDouble(JSONObject jsonObject, String key) {
		double result = 0L;

		if (jsonObject!=null)
		{
			try {
				result = jsonObject.getDouble(key);
			} catch (JSONException e) {
				logger.log(Level.WARNING, "getInfoAsLong  - missing json attribute: " + key);
			}
		}
		return result;
	}
}
