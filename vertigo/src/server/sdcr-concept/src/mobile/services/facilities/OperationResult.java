package mobile.services.facilities;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OperationResult {
	
	private int applicationStatus ;
	private String applicationMessage ;
	private Map<String, String> applicationContext ;
	private Object applicationData ;
	
	public OperationResult(Exception e) {
		
		this.applicationStatus = 500 ;
		this.applicationMessage = e.getMessage() ;
		this.applicationData = null ;
	}
	
	public OperationResult(VertigoException fe) {
		
		this.applicationStatus = fe.getResponseCode() ;
		this.applicationMessage = fe.getMessage() ;
		this.applicationData = null ;
	}
	
	public OperationResult(Object data) {

		this.applicationStatus = 0 ;
		this.applicationMessage = "ok" ;
		this.applicationData = data ;
	}
	
	public OperationResult(Object data, String message) {

		this.applicationStatus = 0 ;
		this.applicationMessage = message;
		this.applicationData = data ;
	}
	
	public String toJsonString()	
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String data = gson.toJson(this);
		return data ;
	}
	
	public String toString() 
	{
		String shortString=this.getShortString(); 
		if ( shortString!=null)
		{			
			return "Error Code: " + this.applicationStatus + " - " + this.applicationMessage + " - Data: " + shortString;
		}
		else
		{
			return "Error Code: " + this.applicationStatus + " - " + this.applicationMessage;
		}
	}
	
	private String getShortString() 
	{
		String shortString = this.getStringValue();
		if ( shortString!=null && shortString.length() > 250 )
		{
			//We have to pay when the log content exceeds some google app engine quotas!!! 
			shortString = shortString.substring(0, 250) + "...";
		}
		return shortString ;
	}

	public String getStringValue() 
	{
		return (this.applicationData!=null)?this.applicationData.toString():null; 
	}

	public int getApplicationStatus() 
	{
		return this.applicationStatus;
	}

	public Object getApplicationData() 
	{
		return this.applicationData;
	}

	public void AddProperty(String propertyName, String propertyValue) 
	{
		if ( applicationContext==null )
		{
			applicationContext = new HashMap<String, String>();
		}
		
		applicationContext.put(propertyName, propertyValue);
	}

}
