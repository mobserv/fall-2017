package mobile.services.facilities;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

public class Helper 
{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(Helper.class.getName());
	
	public static int getCurrentYear() 
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		return calendar.get(Calendar.YEAR);
	}
}
