package mobile.services.facilities;

@SuppressWarnings("serial")
public class VertigoException extends Exception {

	private int responseCode;
	
	public VertigoException(String message, Exception e)
	{
		super(message, e);
		this.responseCode = 500;
	}
	
	public VertigoException(String message)
	{
		super(message);
		this.responseCode = 500;
	}

	public VertigoException(String message, int responseCode) {
		
		super(message);
		this.responseCode = responseCode;
	}
	
	public int getResponseCode()
	{
		return this.responseCode;
	}
}
