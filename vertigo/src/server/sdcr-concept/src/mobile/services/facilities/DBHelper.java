package mobile.services.facilities;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;

import mobile.services.providers.vertigo.VertigoDB;

public class DBHelper 
{
	private static final Logger logger = Logger.getLogger(VertigoDB.class.getName());
	
	public static String getInfoAsString(Entity entity, String key) {
		
		String result = null;

		try {
			result = (String) entity.getProperty(key);
		} catch (Exception e) {
			logger.log(Level.WARNING, "getEntityPropertyAsString - missing property: " + key);
		}

		return result;
	}

	public static int getInfoAsInt(Entity entity, String key) {
		int result = 0;

		try 
		{
			//@todo - this code sounds strange, stupid and slow
			Object intValue = entity.getProperty(key);
			result = Integer.parseInt(intValue.toString());
		} catch (Exception e) {
			logger.log(Level.WARNING, "getEntityPropertyAsInt- missing property: " + key);
		}

		return result;
	}
}
