package mobile.services.facilities;

@SuppressWarnings("serial")
public class VertigoUnexpectedFormatException extends VertigoException {

	public VertigoUnexpectedFormatException(String message) {
		super(message);
	}
	
	public VertigoUnexpectedFormatException(String message, Exception e) {
		super(message, e);
	}
}
