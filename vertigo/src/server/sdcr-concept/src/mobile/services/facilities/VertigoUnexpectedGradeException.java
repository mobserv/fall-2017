package mobile.services.facilities;

@SuppressWarnings("serial")
public class VertigoUnexpectedGradeException extends VertigoException {

	public VertigoUnexpectedGradeException(String message) {
		super(message);
	}
	
	public VertigoUnexpectedGradeException(String message, Exception e) {
		super(message, e);
	}
}
