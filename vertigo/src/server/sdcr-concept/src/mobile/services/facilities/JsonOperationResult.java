package mobile.services.facilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonOperationResult extends OperationResult 
{
	private static transient final String PLACE_HOLDER = "__PLACE_HOLDER__";
	private transient String jsonObjectAsString ;

	public JsonOperationResult(String jsonObjectAsString) 
	{
		super(JsonOperationResult.PLACE_HOLDER);
		this.jsonObjectAsString = jsonObjectAsString ;
	}
	
	@Override
	public String toJsonString() 
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		String data = gson.toJson(this);
		//---------------------------------------------------------------------------------------------
		// Normally we should define a custom serializer! 
		// c.f: https://stackoverflow.com/questions/43813354/gson-custom-serialization-with-annotations
		//---------------------------------------------------------------------------------------------
		// Here we use a nasty trick instead - we assume the jsonString is a valid json string and that  
		// it is already properly encoded to support "exotic" languages! 
		//---------------------------------------------------------------------------------------------
		data = data.replaceFirst("\"" + PLACE_HOLDER + "\"", this.jsonObjectAsString);
		return data ;
	}
	
	@Override
	public String getStringValue() 
	{
		return this.jsonObjectAsString; 
	}
}
