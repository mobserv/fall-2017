package mobile.services.model;

import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

import mobile.services.entities.EClimber;
import mobile.services.facilities.DBHelper;
import mobile.services.facilities.JsonHelper;

@SuppressWarnings("unused")
public class Climber {

	//private static final Logger logger = Logger.getLogger(Climber.class.getName());
	
	private Key key;
	private String xID;
	private String firstName;
	private String lastName;
	private String city;
	private String country;
	private int sex;
	private int height;
	private int weight;
	private int started;
	private String avatar;
	
	private transient String password;  //Not visible in JSON Output
	private String email;
	private String preferredMode;

	public Climber() {
	}

	public Climber initialize(EClimber climber) {
		
		this.xID = climber.getExternalReference();  //8a.nu ID
		
		this.firstName = climber.getFirstName();
		this.lastName = climber.getLastName();
		this.city = climber.getCityName();
		this.country = climber.getCountryCode();
		this.sex = climber.getSex();
		this.height= climber.getHeight();
		this.weight= climber.getWeight();
		//this.started= climber.getStarted);
		this.avatar = climber.getAvatar();
		
		this.email = climber.getEMail();
		this.password = climber.getPassword();
		
		this.preferredMode = climber.getPreferredMode();
		return this;
	}
	
	public Climber initialize(JSONObject source) {
		
		this.key = null; //Vertigo ID 
		
		this.xID = JsonHelper.getInfoAsString(source, "id");  //8a.nu ID
		this.firstName = JsonHelper.getInfoAsString(source, "firstName");
		this.lastName = JsonHelper.getInfoAsString(source, "lastName");
		this.city = JsonHelper.getInfoAsString(source, "city");
		this.country = JsonHelper.getInfoAsString(source, "country");
		this.sex = JsonHelper.getInfoAsInt(source, "sex");
		this.height= JsonHelper.getInfoAsInt(source, "height");
		this.weight= JsonHelper.getInfoAsInt(source, "weight");
		this.started= JsonHelper.getInfoAsInt(source, "started");
		
		this.avatar = JsonHelper.getInfoAsString(source, "avatar");

		// External sources are not supposed to expose this information
		if (source.has("email")) 
			this.email = JsonHelper.getInfoAsString(source, "email");
		if (source.has("password")) 
			this.password = JsonHelper.getInfoAsString(source, "password");
		
		return this;
	}
	
	@Override
	public String toString()
	{
		return "Climber: "+ this.key + " - " + this.firstName + " " + this.lastName + "- ["+ this.xID + "]";
	}

	public Entity toEntity(Entity climber) 
	{
		//--------------------------------------------------------------------
		//@todo - use Objectify Annotations to directly persist class members
		//--------------------------------------------------------------------
		if ( climber==null)
		{
			climber = new Entity("Climber");
		}
		else
		{
			//Here we are about to update an existing entity!
		}
		
		climber.setIndexedProperty("xID", this.xID);
		
		climber.setIndexedProperty("email", this.email);
		climber.setProperty("password", this.password);
		
		climber.setProperty("firstName", this.firstName);
		climber.setProperty("lastName", this.lastName);
		
		climber.setProperty("city", this.city);
		climber.setProperty("country", this.country);
		climber.setProperty("sex", this.sex);
		climber.setProperty("height", this.height);
		climber.setProperty("weight", this.weight);
		climber.setProperty("started", this.started);
		climber.setProperty("avatar", this.avatar);
		
		return climber;
	}

	public Object getXID() 
	{
		return this.xID;
	}

	public Climber setData(HttpServletRequest req) {

		String value = req.getParameter("email");
		if ( notEmpty(value) ) this.email = value ;
		
		value = req.getParameter("password");
		if ( notEmpty(value) ) this.password = value ;
		
		value = req.getParameter("firstName");
		if ( notEmpty(value) ) this.firstName = value ;
		
		value = req.getParameter("lastName");
		if ( notEmpty(value) ) this.lastName = value ;
		
		this.xID = req.getParameter("xID");
		
		return this;
	}

	private boolean notEmpty(String value) {
		
		if ( value==null || value.isEmpty() ) return false;
		return true ;
	}

	public String getPassword() 
	{
		return this.password;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getExternalReference() {
		return this.xID;
	}

	public String getEMail() {
		return this.email;
	}

	public String getCityName() {
		return this.city;
	}

	public String getCountryCode() {
		return this.country;
	}

	public Integer getSex() {
		return this.sex;
	}

	public Integer getHeight() {
		return this.height;
	}

	public Integer getWeight() {
		return this.weight;
	}

	public String getAvatar() {
		return this.avatar;
	}

	public String getPreferredMode() {
		return this.preferredMode;
	}
}
