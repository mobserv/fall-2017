package mobile.services.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.google.appengine.repackaged.com.google.gson.Gson;
//import com.google.appengine.repackaged.com.google.gson.GsonBuilder;

import mobile.services.facilities.VertigoException;
import mobile.services.facilities.VertigoUnexpectedFormatException;

public class Grades {
	
	private volatile static Grades instance = null;  //volatile to be MT-Safe
	
	private HashMap<Integer, Grade> GRADES = null;
	private ArrayList<Grade> ORDERED_GRADES = null;

	protected Grades() {
	}

	public static Grades getInstance() {
		
		if (instance == null) {
			synchronized (Grades.class) {
				if (instance == null) {  //Double-check to be MT-Safe!
					instance = new Grades();
					
					instance.init();
				}
			}
		}
		return instance;
	}

	private void init() {
		this.declareGrades();
	}

	private void declareGrades() {

		if (this.GRADES == null) {
			this.GRADES = new HashMap<Integer, Grade>();
			this.ORDERED_GRADES = new ArrayList<Grade>();
			
			@SuppressWarnings("unused")
			int count;
			
			count = this.declareGrade(21, "4a", 200 );
			count = this.declareGrade(23, "4b", 210 );
			count = this.declareGrade(25, "4c", 230  );
			
			count = this.declareGrade(29, "5a", 250 );
			count = this.declareGrade(31, "5b", 275 );  
			count = this.declareGrade(33, "5c", 350 );
			
			count = this.declareGrade(36, "6a", 400 );
			count = this.declareGrade(38, "6a+", 450 );
			count = this.declareGrade(40, "6b", 500 );
			count = this.declareGrade(42, "6b+", 550 );
			count = this.declareGrade(44, "6c", 600 );
			count = this.declareGrade(46, "6c+", 650 );
			
			count = this.declareGrade(49, "7a", 700 );  //Odd - Why?
			count = this.declareGrade(51, "7a+", 750 );
			count = this.declareGrade(53, "7b", 800 );
			count = this.declareGrade(55, "7b+", 850 );
			count = this.declareGrade(57, "7c", 900 );
			count = this.declareGrade(59, "7c+", 950 );
			
			count = this.declareGrade(62, "8a", 1000 );  //? even again - why?
			count = this.declareGrade(64, "8a+", 1050 );
			count = this.declareGrade(66, "8b", 1100 ); 
			count = this.declareGrade(68, "8b+", 1150 );
			count = this.declareGrade(70, "8c", 1200 ); 
			count = this.declareGrade(72, "8c+", 1250 );  
			
			count = this.declareGrade(75, "9a", 1300 ); //odd again - !?!
			count = this.declareGrade(77, "9a+", 1350 );
			count = this.declareGrade(79, "9b", 1400 );
			count = this.declareGrade(81, "9b+", 1450 );
			count = this.declareGrade(83, "9c", 1500 );
			count = this.declareGrade(85, "9c+", 1550 );
		}
	}

	private int declareGrade(int index, String text, int credits) {

		Grade grade = new Grade(index, text, credits);
		this.GRADES.put(grade.getIndex(), grade);
		this.ORDERED_GRADES.add(grade);
		
		return this.GRADES.size();
	}

	public Grade getGrade(int gradeIndex) {
		
		return this.GRADES.get(gradeIndex);
	}

	public String getGradesAsJsonString() {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this.ORDERED_GRADES);
	}

	public Grade getGrade(String gradeAsString) throws VertigoException 
	{
		Grade result=null ;
		for ( Grade grade: this.ORDERED_GRADES )
		{
			if ( grade.getText().equals(gradeAsString) )
			{
				result = grade;
				break;
			}
		}
		
		if ( result == null )
		{
			throw new VertigoUnexpectedFormatException("Grade is inconsistent: "+gradeAsString);
		}
		
		return result;
	}
}