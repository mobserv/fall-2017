package mobile.services.model;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

import mobile.services.entities.ECrag;
import mobile.services.facilities.JsonHelper;

public class Crag 
{
	@SuppressWarnings("unused")
	private String name;
	private String xID;
	private String city;
	private String countryCode;
	
	private transient JSONObject location;
	
	private double longitude;
	private double latitude;
	
	private transient String comment;

	public Crag initialize(JSONObject ascentObject) 
	{
		this.xID = JsonHelper.getInfoAsString(ascentObject, "id");
		this.name = JsonHelper.getInfoAsString(ascentObject, "name");
		this.city = JsonHelper.getInfoAsString(ascentObject, "city");
		this.comment = JsonHelper.getInfoAsString(ascentObject, "cragComment");

		this.location = JsonHelper.getInfoAsObject(ascentObject, "location");
		this.longitude = JsonHelper.getInfoAsDouble(this.location, "lon");
		this.latitude= JsonHelper.getInfoAsDouble(this.location, "lat");

		return this;
	}

	public Crag initialize(ECrag entity) {
		
		this.xID = entity.getExternalReference();
		this.name = entity.getName();
		this.city = entity.getCity();
		this.comment = entity.getComment();

		this.longitude = entity.getLongitude();
		this.latitude= entity.getLatitude();
		
		this.countryCode = entity.getCountryCode();
		return this;
	}

	public String getCity() 
	{
		return this.city;
	}

	public String getExternalReference() 
	{
		return this.xID;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public String getComment() {
		return this.comment;
	}
	
	public String getCountryCode() {
		return this.countryCode;
	}
}
