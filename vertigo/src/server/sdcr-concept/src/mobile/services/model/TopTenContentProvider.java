package mobile.services.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import mobile.services.facilities.Helper;
import mobile.services.facilities.VertigoException;
import mobile.services.providers.IContentProvider;
import mobile.services.providers.h8a.H8aFacade;

public class TopTenContentProvider {

	private static final Logger logger = Logger.getLogger(TopTenContentProvider.class.getName());

	private transient IContentProvider provider;
	private transient HashMap<String, ArrayList<Ascent>> allAscentsPerYear = new HashMap<String, ArrayList<Ascent>>();
	private HashMap<String, List<Ascent>> topTenAscentsPerYear = new HashMap<String, List<Ascent>>();
	
	public TopTenContentProvider(IContentProvider provider) {
	  this.provider = provider;	
	}

	public TopTenContentProvider organizeData() throws VertigoException 
	{
		ArrayList<Ascent> ascents = provider.getAscents();
		
		//ascents.sort(Facade8a.getInstance().getByScoreComparer()); - ArrayList.NoSuchMethodError on GAE Standard Environment
		Collections.sort(ascents, H8aFacade.getInstance().getByScoreComparer());
		allAscentsPerYear.put("all", ascents);
		
		for ( Ascent ascent: ascents) {
			this.dispatchAscent(ascent);
		}
		
		this.organize();
		this.shrink();
		
		return this;  //Just to allow chaining
	}
	
	public void populateData(ArrayList<Ascent> ascents) {

	}

	private void dispatchAscent(Ascent ascent) 
	{
		String year = ascent.getYear();
		this.updateFirstYear(year);
		
		ArrayList<Ascent> yAscents = _ascentsForYear(year);
		yAscents.add(ascent);
		
		if ( !ascent.moreThan12MonthsAgo() )
		{
			yAscents = _ascentsForYear("now");
			yAscents.add(ascent);
		}
	}
	
	private void organize() {
		
		for (ArrayList<Ascent> ascents : allAscentsPerYear.values()) {
			//ascents.sort(Facade8a.getInstance().getByScoreComparer()); - ArrayList.NoSuchMethodError on GAE Standard Environment
			Collections.sort(ascents, H8aFacade.getInstance().getByScoreComparer());
		}		
	}
	
	private void shrink() {
		
		//The actual score takes into account only the 10 best performance
		for (Entry<String, ArrayList<Ascent>> entry: allAscentsPerYear.entrySet()) {
			
			String year = entry.getKey();
			ArrayList<Ascent> allYearAscent = entry.getValue();
			List<Ascent> topTen = allYearAscent.subList(0, Math.min(10, allYearAscent.size()));
			
			this.topTenAscentsPerYear.put(year, topTen);
		}		
	}	

	private ArrayList<Ascent> _ascentsForYear(String year) {
		
		ArrayList<Ascent> yAscents = allAscentsPerYear.get(year);
		if ( yAscents == null )
		{
			yAscents = new ArrayList<Ascent>();
			allAscentsPerYear.put(year, yAscents);
		}
		
		return yAscents;
	}

	public ArrayList<Ascent> getAscents(String year) 
	{
		return allAscentsPerYear.get(year);
	}

	private transient int firstYear = 0;
	
	private void updateFirstYear(String yearAsString) {
		try
		{
			int year = Integer.parseInt(yearAsString);
			if ( firstYear==0 || year<firstYear )
			{
				firstYear = year;
			}
		}
		catch (Exception e)
		{
			logger.log(Level.WARNING, "updateFirstYear - unexpected value: " + yearAsString);
		}
	}

	public int getFirstAscentYear() 
	{
		if ( firstYear > 0 )
		{
			return firstYear;
		}
		else 
		{
			return Helper.getCurrentYear();
		}
	}

	public int computeScoreForYear(String year) 
	{
		int score = 0;
		
		List<Ascent> ascentList = topTenAscentsPerYear.get(year);
		if ( ascentList !=null )
		{
			for ( int i=0; i<Math.min(10, ascentList.size()); i++)
			{
				score += ascentList.get(i).getScore();
			}
		}		
		return score;
		
	}

	public HashMap<String, List<Ascent>> GetTopTenCollections() 
	{
		return this.topTenAscentsPerYear;
	}

	public Climber getClimber() throws VertigoException 
	{
		return this.provider.getClimber();
	}

}
