package mobile.services.model;

public class Grade {
	
	private int index;
	private int credits;
	private String text;

	public Grade()
	{
	}
	
	public Grade(int index, String text, int credits )
	{
		this.index = index;
		this.credits = credits;
		this.text = text; 
	}

	public int getIndex() {
		return index;
	}

	public int getCredits() {
		return credits;
	}
	
	public String getText() {
		return text;
	}
	
//	public JSONObject ToJsonObject()
//	{
//		JSONObject jo = new JSONObject();
//		try {
//			jo.put("index", this.getIndex());
//			jo.put("text", this.getText());
//			jo.put("credits", this.getCredits());
//			return jo;
//		} 
//		catch (JSONException e) {
//			throw new UnsupportedOperationException("Unexpected situation - Grade could not be serialized");
//		}
//	}
	
	public String ToString()
	{
		return this.Dump();
	}
	
	public String Dump()
	{
		return "Index (8a.nu): " + this.getIndex() + " = Grade: " + this.getText() + " - Credits: (" + this.getCredits() + ")";
	}
}
