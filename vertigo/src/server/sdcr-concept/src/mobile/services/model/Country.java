package mobile.services.model;

import mobile.services.entities.ECountry;

public class Country 
{
	private String code;
	
	public Country initialize(ECountry entity) {
	
		this.code = entity.getCode();
		return this;
	}

	public String getCode() {
		return this.code;
	}
}
