package mobile.services.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

import mobile.services.entities.EAscent;
import mobile.services.entities.EClimber;
import mobile.services.facilities.JsonHelper;
import mobile.services.facilities.VertigoException;
import mobile.services.facilities.VertigoUnexpectedFormatException;
import mobile.services.facilities.VertigoUnexpectedGradeException;

public class Ascent {

	private static final Logger logger = Logger.getLogger(Ascent.class.getName());

	private String climberName;
	private String climberExternalReference;
	
	private String name;
	private String crag;
	private String sector;
	private String country;
	
	private transient int gradeIndex;
	private transient int methodIndex;
	
	@SuppressWarnings("unused")
	private String method;
	
	@SuppressWarnings("unused")
	private String grade;
	
	private int score;

	private transient String date_as8a;
	private transient Date ascentDate;

	private String date;  //date exposed via json as string
	
	public Ascent()
	{
		
	}
	
	public Ascent initialize(EAscent entity) throws VertigoException
	{
		this.name = entity.getRouteName();
		
		this.crag = entity.getCragName();
		this.sector = entity.getSectorName();
		this.country = entity.getCountryCode();
		
		this.gradeIndex = entity.getGradeIndex();
		this.grade = this.convertGradeIndex(this.gradeIndex);
		
		this.methodIndex = entity.getMethodIndex();
		this.method = this.convertMethodIndex(this.methodIndex);
		
		this.score = entity.getScore();
		
		this.ascentDate = entity.getAscentDate();
		this.date_as8a = "" + this.ascentDate.getTime() / 1000;
		this.date = this.convertHumanReadableDate(ascentDate);
		
		return this;
	}

	public Ascent initialize(JSONObject ascentObject) throws VertigoException 
	{
		this.name = JsonHelper.getInfoAsString(ascentObject, "name");
		
		this.crag = JsonHelper.getInfoAsString(ascentObject, "crag");
		this.sector = JsonHelper.getInfoAsString(ascentObject, "sector");
		this.country = JsonHelper.getInfoAsString(ascentObject, "country");
		
		this.gradeIndex = JsonHelper.getInfoAsInt(ascentObject, "grade");
		this.grade = this.convertGradeIndex(this.gradeIndex);
		
		this.methodIndex = JsonHelper.getInfoAsInt(ascentObject, "methodId");
		this.method = this.convertMethodIndex(this.methodIndex);
		
		this.score = JsonHelper.getInfoAsInt(ascentObject, "totalScore");
		
		this.date_as8a = JsonHelper.getInfoAsString(ascentObject, "date");
		this.ascentDate = this.convertToDate(this.date_as8a);
		this.date = this.convertHumanReadableDate(this.ascentDate);
		
		return this;
	}

	public Ascent initialize(HttpServletRequest request) throws VertigoException 
	{
		this.name = request.getParameter("name");
		this.crag = request.getParameter("crag");
		this.sector = request.getParameter("sector");
		this.country = request.getParameter("country");
		
		if ( this.name==null || this.name.isEmpty())
		{
			throw new VertigoException("Route name is empty");
		}
		if ( this.crag==null || this.crag.isEmpty())
		{
			throw new VertigoException("Crag name is empty");
		}
		if ( this.country==null || this.country.isEmpty())
		{
			throw new VertigoException("Country is empty");
		}
		
		Grade grade = Grades.getInstance().getGrade(request.getParameter("grade"));
		this.grade = grade.getText();
		this.gradeIndex = grade.getIndex();
		
		this.method = request.getParameter("method");
		this.methodIndex = this.getMethodIndex(this.method);
		
		this.score = this.computeScore(grade);
		
		String date_asAndroid = request.getParameter("date"); //##CR - to be clarified
		this.ascentDate = this.convertSimpleDate(date_asAndroid, false);
		this.date_as8a = "" + this.ascentDate.getTime() / 1000;
		this.date = this.convertHumanReadableDate(ascentDate);
		
		return this;
	}
	
	private Date convertSimpleDate(String date_asAndroid, boolean strictMode) throws VertigoException 
	{
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

        try 
        {
            return df.parse(date_asAndroid);
        } 
        catch (ParseException e) 
        {
        	if ( strictMode )
        	{
        		throw new VertigoUnexpectedFormatException("Unexpected date format: "+date_asAndroid);
        	}
        	else
        	{
        		//##CR - workaround to incorrect date format send by android app
        		return new Date();
        	}
        }
	}

	private int computeScore(Grade grade ) throws VertigoException
	{
		return grade.getCredits() + this.computeBonus(); 
	}

	private int computeBonus() throws VertigoException
	{
		if ( methodIndex==3 )
		{
			return 100;
		}
		else if ( methodIndex==2 )
		{
			return 50;
		}
		else if ( methodIndex==1 )
		{
			return 0;
		}
		else
		{
			throw new VertigoUnexpectedFormatException("Unexpected method format: "+method);	
		}
	}

	private String convertMethodIndex(int methodIndex) {
		
		if ( methodIndex==3 )
		{
			return "On Sight";
		}
		else if ( methodIndex==2 )
		{
			return "Flash";
		}
		else if ( methodIndex==1 )
		{
			return "Red Point";
		}
		else
		{
			return "N/A";
		}
	}
	
	private int getMethodIndex(String method) throws VertigoException 
	{
		if ( method.equals("OS") )
		{
			return 3;
		}
		else if ( method.equals("FL") )
		{
			return 2;
		}
		else if ( method.equals("RP") )
		{
			return 1;
		}
		else
		{
			throw new VertigoUnexpectedFormatException("Unexpected method format: "+method);
		}
	}

	private String convertGradeIndex(int gradeIndex) throws VertigoException {
		
		Grade grade = Grades.getInstance().getGrade(gradeIndex);
		if ( grade==null )
		{
			throw new VertigoUnexpectedGradeException( "Unexpected grade index: " +gradeIndex );
		}
		return grade.getText();
	}

	private Date convertToDate(String date_as8a) {
	
		Date date = null;
		
	    try 
	    {
	    	long seconds = Long.parseLong(date_as8a);  //e.g.: 979426800
	    	date = new Date(seconds*1000);
	    }
	    catch (Exception e) 
	    {
	    	logger.log(Level.WARNING, date_as8a + " is not a valid epoc number.");
	    }
	    
	    return date;
	}

    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private String convertHumanReadableDate(Date ascentDate) {
		
		String dateAsString = null;
		
	    try 
	    {
	    	DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
	    	dateAsString = DATE_FORMAT.format(ascentDate);
	    }
	    catch (Exception e) 
	    {
	    	logger.log(Level.WARNING, "" + ascentDate + " is not a valid epoc number.");
	    }
	    
	    return dateAsString ;
	}


	public int getScore() {
		return this.score;
	}

	public boolean isDoneBefore1998() {
		
		String yearAsString = this.getYear();
		try
		{
			int year = Integer.parseInt(yearAsString);
			if ( year < 1998 )
			{
				return true ;
			}
		}
		catch (Exception e)
		{
			logger.log(Level.WARNING, "isDoneBefore1990 - unexpected value: " + yearAsString);
		}
		return false;
	}
	
	public boolean moreThan12MonthsAgo() {
		
		
		Date date = new Date();
		long time = date.getTime();
		long ascentTime = Long.parseLong(this.date_as8a) * 1000;
		long delta = time - ascentTime;
		long yearMillisSeconds = (long)365 * 24 * 60 * 60 * 1000;
		
		if ( delta < yearMillisSeconds )  //One year!
		{
			return false;
		}
		else
		{
			return true ;
		}
	}

	public String getYear() 
	{
		return this.date.substring(0,  4);
	}

	public int getDateAsInt() 
	{
		return Integer.parseInt(this.date_as8a);
	}

	public String getCountryCode() 
	{
		return country;
	}
	
	public String getCragName() 
	{
		return crag;
	}

	public String getRouteName() {
		return name;
	}

	public String getSectorName() {
		return sector;
	}

	public Integer getGradeIndex() {
		return gradeIndex;
	}

	public Integer getStyle() {
		return methodIndex;
	}

	public Date getAscentDate() {
		return ascentDate;
	}

	public void enrichWith(EClimber climberEntity) {
		this.climberName = climberEntity.getFullName();
		this.climberExternalReference = climberEntity.getExternalReference();
	}

}
